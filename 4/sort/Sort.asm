
// sort: variation of bubble sort

// initialize

@i
M = 0
@x
M = 0
@j
M = 0
@address
M = 0
@len
M = 0
@temp
M = 0
@MAIN
0;JMP

// swap if value at address pointer by x smaller than value at address x+1
// pointer by y then swap, else do nothing
(SWAP)
	
	@x
	A = M
	D = M
	A = A + 1

	D = M - D
	
	@INNER_END
	D;JLE

	// temp = x
	@x
	A = M
	D = M
	@temp
	M = D

	// x = y
	@x
	A = M + 1
	D = M
	A = A - 1
	M = D

	// y = x
	@temp
	D = M
	@x
	A = M + 1
	M = D

(INNER_END)
	@INNER_LOOP_CALL_END
	0;JMP	
	
(END_SWAP)


(MAIN)
	@i
	M = 0
	@R14
	D = M
	@address
	M = D

	(LOOP)
		
		@i
		D = M
		@R15
		D = M - D
		D = D - 1
		@END_LOOP
		D;JEQ

		@len
		M = D 

		@j
		M = 0

		(INNER_LOOP)

			@j
			D = M
			@len
			D = M - D

			@END_INNER_LOOP
			D;JEQ

			@j
			D = M
			@address
			D = M + D
			@x
			M = D

			@SWAP
			0;JMP

		(INNER_LOOP_CALL_END)
			A = 0
			@j
			M = M + 1
			@INNER_LOOP
			0;JMP

		(END_INNER_LOOP)

		@i
		M = M + 1
		@LOOP
		0;JMP

	(END_LOOP)

(END_MAIN)
	@END_MAIN
	0;JMP