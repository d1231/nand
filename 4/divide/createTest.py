import random
COUNT = 200
DIVISOR_UP_LIMIT = 100
DIVIDEND_UP_LIMIT = 300
testStr = "set PC 0,\nset RAM[13] %(dividend)s,   // Set test arguments\nset RAM[14] %(divisor)s,\nset RAM[15] -1;  // Ensure that program initialized product to 0\nrepeat 400 {\t\nticktock;\n}\noutput;\n\n"
finalTestStr = "output-file Div.out,\ncompare-to cmp.cmp,\noutput-list RAM[13]%D2.6.2 RAM[14]%D2.6.2 RAM[15]%D2.6.2;"
finalCmpStr = "| RAM[13]  | RAM[14]  | RAM[15]  |\n"
for i in range(COUNT):
	divis = random.randint(1, DIVISOR_UP_LIMIT)
	divid = random.randint(divis, DIVIDEND_UP_LIMIT)
	result = int(divid/divis)
	finalTestStr += testStr %{"dividend": str(divid), "divisor": str(divis)}
	finalCmpStr += "|" + str(divid).rjust(8) + "  |" + str(divis).rjust(8) + "  |" + str(result).rjust(8) + "  |\n"

with open("tests.tst","w") as resFile:
    resFile.write(finalTestStr);
with open("cmp.cmp","w") as resFile:
    resFile.write(finalCmpStr);
	
