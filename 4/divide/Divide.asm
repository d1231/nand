
// divsion: long division with base 2

// initializing variables

@R13
D = M
@dividend
M = D

@R14
D = M
@divisor
M = D

@R15
M = 0

@quotient
M = 0

@MAIN
0;JMP

// returns the minimum number of bits that are required to 
// repsent the number without changing the value
(SIZE)

	@x
	D = M

	@i
	M = 0

(LOOP_SIZE)
	
	@LOOP_SIZE_END
	D;JEQ

	@i
	M = M + 1
	D = D>>

	@LOOP_SIZE
	0;JMP

(LOOP_SIZE_END)

	@i
	D = M

	@size_ret
	M = D
	
	@address
	A = M
	0;JMP

(MAIN)

	// compute the number of bits of divisor
	@SIZE_RETURN_1
	D = A

	@address
	M = D

	@divisor
	D = M

	@x
	M = D

	@SIZE
	0;JMP

(SIZE_RETURN_1)

	@size_ret
	D = M
	@divisor_size
	M = D
	
	// compute the number of bits in dividend
	@SIZE_RETURN_2
	D = A

	@address
	M = D

	@dividend
	D = M

	@x
	M = D

	@SIZE
	0;JMP

(SIZE_RETURN_2)

	@size_ret
	D = M
	D = D + 1

	@divisor_size
	D = D - M

	// if loop size < 0 then divisor > dividend so finish with result = 0
	@END_LOOP_2
	D;JLT

	@loop_size
	M = D

@i
M = 0

// align divisor with dividend
(LOOP_1)
	
	@loop_size
	D = M 

	@i
	D = D - M
	D = D - 1

	@END_LOOP_1
	D;JEQ

	@divisor
	M = M<<

	@i
	M = M + 1

	@LOOP_1
	0;JMP

(END_LOOP_1)



@i
M = 0

// compute the quotient, long division style
(LOOP_2)

	@loop_size
	D = M

	@i
	D = M - D
	@END_LOOP_2
	D;JEQ

	// shift left the quotient
	@quotient
	M = M<<

	@divisor
	D = M

	// divisor - dividend > 0
	@dividend
	D = D - M

	// jump if divisor > dividend 
	@IF_SMALLER
	D;JGT

	@dividend
	M = -D

	@quotient
	M = M + 1

(IF_SMALLER)
	
	@divisor
	M = M>>

	@i
	M = M + 1

	@LOOP_2
	0;JMP

(END_LOOP_2)

	@quotient
	D = M
	@R15
	M = D
