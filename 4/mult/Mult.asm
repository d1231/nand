// This file is part of the materials accompanying the book 
// "The Elements of Computing Systems" by Nisan and Schocken, 
// MIT Press. Book site: www.idc.ac.il/tecs
// File name: projects/04/res.asm

// simple multiplication: add (R2) times R1

// initilaize variables

@R1
D = M

@i
M = D

@R2
M = 0

// loop start
(LOOP)
	
	// condition
	@i
	D = M

	@END
	D;JEQ // if (R1-D) = 0 jump to end

	// iteration body
	@R0
	D = M

	@R2
	M = M + D

	// update iteration
	@i
	M = M - 1

	@LOOP
	0;JMP

(END)