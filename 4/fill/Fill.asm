// This file is part of the materials accompanying the book 
// "The Elements of Computing Systems" by Nisan and Schocken, 
// MIT Press. Book site: www.idc.ac.il/tecs
// File name: projects/04/Fill.asm


(MAIN)

	@previous
	M = 0

	(MAIN_LOOP)

		@KEY_CHECK
		0;JMP

	(KEY_CHECK_CALL_END)

		@pixel
		M = D

		@previous
		D = D - M

		// if previous == 0 then previous input wasn't changed, check again
		@MAIN_LOOP
		D;JEQ

		@pixel
		D = M

		@previous
		M = D

		@DRAW_SCREEN
		0;JMP

	(DRAW_SCREEN_CALL_END)

		@MAIN_LOOP
		0;JMP

// end main


// key_check: if key is pressed then D=-1, else D=0
(KEY_CHECK)

	@KBD
	D = M

	@KEY_PRESSED
	D;JNE

	D = 0

(KEYCHECK_END)

	@KEY_CHECK_CALL_END
	0;JMP

(KEY_PRESSED)

	D = -1
	@KEYCHECK_END
	0;JMP

// key_check end

// draw_screen: draw screen with value at pixel
(DRAW_SCREEN)

	@SCREEN
	D = A

	@i
	M = D

	(DRAW_SCREEN_LOOP)

		// check condition
		@i
		D = M

		@KBD
		D = D - A

		@DRAW_SCREEN_LOOP_END
		D;JGT

		@pixel
		D = M 

		@i
		A = M
		M = D

		@i
		M = M + 1

		@DRAW_SCREEN_LOOP
		0;JMP

	(DRAW_SCREEN_LOOP_END)
		@DRAW_SCREEN_CALL_END
		0;JMP
		
// draw_screen end

