package jack.analyzer.variable_deceleration;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineVariableDeceleration implements JackElement {

	public static final String VAR_DEC = "varDec";

	private final JackToken type;

	private final BaseVariableDeceleration variableDeceleration;


	public SubroutineVariableDeceleration(JackToken type, BaseVariableDeceleration variableDeceleration) {

		this.type = type;
		this.variableDeceleration = variableDeceleration;
	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(VAR_DEC);

		type.writeXML(writer);
		variableDeceleration.writeXML(writer);

		writer.endTag(VAR_DEC);

	}

	@Override
	public String toString() {

		return "SubroutineVariableDeceleration{" +
				"type=" + type +
				", variableDeceleration=" + variableDeceleration +
				'}';
	}
}
