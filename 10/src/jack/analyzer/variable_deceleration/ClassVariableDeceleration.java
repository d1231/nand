package jack.analyzer.variable_deceleration;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class ClassVariableDeceleration implements JackElement {

	public static final String CLASS_VAR_DEC = "classVarDec";

	private final JackToken type;

	private final BaseVariableDeceleration variableDeceleration;


	public ClassVariableDeceleration(JackToken type, BaseVariableDeceleration variableDeceleration) {

		this.type = type;
		this.variableDeceleration = variableDeceleration;
	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(CLASS_VAR_DEC);

		type.writeXML(writer);
		variableDeceleration.writeXML(writer);

		writer.endTag(CLASS_VAR_DEC);

	}

	@Override
	public String toString() {

		return "ClassVariableDeceleration{" +
				"type=" + type +
				", variableDeceleration=" + variableDeceleration +
				'}';
	}
}
