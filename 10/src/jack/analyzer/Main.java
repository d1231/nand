package jack.analyzer;

import jack.analyzer.utils.JackUtils;

import java.io.File;

/**
 * Created by danny on 10/04/15.
 */
public class Main {


	public static void main(String[] args) {

		if (args.length == 0) {
			System.err.println("Please provide argument");
			return;
		}

		String arg = args[0];

		File[] files = getJackFiles(arg);

		for (File file : files) {

			JackAnalyzer analyzer = new JackAnalyzer(file);
			try {
				analyzer.analyzer();
				System.out.println("Created xml file for: " + file.getName());
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private static File[] getJackFiles(String arg) {

		File file = new File(arg);

		File[] files;

		if (!arg.endsWith(JackUtils.JACK_SUFFIX)) {

			files = file.listFiles((dir, name) -> {
				return name.endsWith(JackUtils.JACK_SUFFIX);
			});


		} else {

			files = new File[] {file};
		}

		return files;

	}
}
