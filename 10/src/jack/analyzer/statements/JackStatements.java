package jack.analyzer.statements;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 19/04/15.
 */
public class JackStatements implements JackElement {

	public static final String STATEMENTS = "statements";

	private final ArrayList<JackStatement> statements;

	public JackStatements(ArrayList<JackStatement> statements) {

		this.statements = statements;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(STATEMENTS);

		for (JackStatement statement : statements) {
			statement.writeXML(writer);
		}

		writer.endTag(STATEMENTS);
	}
}
