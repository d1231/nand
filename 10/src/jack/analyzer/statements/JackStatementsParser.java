package jack.analyzer.statements;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.tokenizer.JackTokenizerException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 19/04/15.
 */
public class JackStatementsParser {

	public static JackStatements parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		ArrayList<JackStatement> statements = new ArrayList<>();

		JackStatement statement;
		while ((statement = JackStatementParser.parse(jackTokenFeeder)) != null) {
			statements.add(statement);
		}

		return new JackStatements(statements);

	}
}
