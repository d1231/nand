package jack.analyzer.statements.statement;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatements;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackSymbol;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class IfStatement implements JackStatement {

	private static final String IF_STATEMENT = "ifStatement";

	private static final JackKeyword IF = JackKeyword.factory("if");

	private static final JackKeyword ELSE = JackKeyword.factory("else");

	private final static JackSymbol openingBracket = JackSymbol.factory('(');

	private final static JackSymbol closingBracket = JackSymbol.factory(')');

	private final static JackSymbol openingCurlyBracket = JackSymbol.factory('{');

	private final static JackSymbol closingCurlyBracket = JackSymbol.factory('}');

	private final JackExpression expression;

	private final JackStatements statements;

	private final JackStatements elseStatements;

	public IfStatement(JackExpression expression, JackStatements statements, JackStatements elseStatements) {

		this.expression = expression;

		this.statements = statements;

		this.elseStatements = elseStatements;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(IF_STATEMENT);

		IF.writeXML(writer);

		openingBracket.writeXML(writer);

		expression.writeXML(writer);

		closingBracket.writeXML(writer);

		openingCurlyBracket.writeXML(writer);

		statements.writeXML(writer);

		closingCurlyBracket.writeXML(writer);

		if (elseStatements != null) {
			ELSE.writeXML(writer);

			openingCurlyBracket.writeXML(writer);

			elseStatements.writeXML(writer);

			closingCurlyBracket.writeXML(writer);
		}

		writer.endTag(IF_STATEMENT);

	}

	@Override
	public String toString() {

		return "IfStatement{" +
				"expression=" + expression +
				", statements=" + statements +
				", elseStatements=" + elseStatements +
				'}';
	}
}
