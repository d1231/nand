package jack.analyzer.statements.statement;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackSymbol;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class ReturnStatement implements JackStatement {

	private static final String RETURN_STATEMENT = "returnStatement";

	private static final JackKeyword RETURN = JackKeyword.factory("return");

	private static final JackSymbol END_TOKEN = JackSymbol.factory(';');

	private final JackExpression expression;

	public ReturnStatement(JackExpression expression) {

		this.expression = expression;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(RETURN_STATEMENT);

		RETURN.writeXML(writer);


		if (expression != null) {
			expression.writeXML(writer);
		}

		END_TOKEN.writeXML(writer);

		writer.endTag(RETURN_STATEMENT);

	}

	@Override
	public String toString() {

		return "ReturnStatement{" +
				"expression=" + expression +
				'}';
	}
}
