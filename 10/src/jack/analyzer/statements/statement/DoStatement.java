package jack.analyzer.statements.statement;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackSymbol;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class DoStatement implements JackStatement {

	public static final String DO_STATEMENT = "doStatement";

	private static final JackKeyword DO = JackKeyword.factory("do");

	private static final JackSymbol END_TOKEN = JackSymbol.factory(';');

	private final JackTerm subroutineCall;

	public DoStatement(JackTerm subroutineCall) {

		this.subroutineCall = subroutineCall;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(DO_STATEMENT);

		DO.writeXML(writer);

		subroutineCall.writeXML(writer);

		END_TOKEN.writeXML(writer);

		writer.endTag(DO_STATEMENT);

	}

	@Override
	public String toString() {

		return "DoStatement{" +
				"subroutineCall=" + subroutineCall +
				'}';
	}
}
