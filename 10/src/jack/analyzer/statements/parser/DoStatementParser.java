package jack.analyzer.statements.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.parser.JackTermParselet;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatementParserException;
import jack.analyzer.statements.statement.DoStatement;
import jack.analyzer.subroutine.call.SubRoutineCallParselet;
import jack.analyzer.subroutine.call.SubroutineInvokeParselet;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class DoStatementParser implements JackStatementParselet {

	private static final JackKeyword DO = JackKeyword.factory("do");

	private final static HashMap<String, JackTermParselet> parselets = new HashMap<String, JackTermParselet>() {{

		put(".", new SubroutineInvokeParselet(false));
		put("(", new SubRoutineCallParselet(false));

	}};


	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!DO.equals(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}


		JackTermParselet parselet = parselets.get(jackTokenFeeder.peekNextNext().getValue());

		if (parselet == null) {
			throw new JackStatementParserException();
		}

		JackTerm subroutineCall = parselet.parse(jackTokenFeeder);

		if (!JackUtils.isEndToken(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		return new DoStatement(subroutineCall);

	}
}
