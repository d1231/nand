package jack.analyzer.statements;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.statements.parser.*;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackStatementParser {

	private final static HashMap<JackToken, JackStatementParselet> parselets = new HashMap<JackToken,
			JackStatementParselet>() {{

		put(JackKeyword.factory("let"), new LetStatementParser());
		put(JackKeyword.factory("if"), new IfStatementParser());
		put(JackKeyword.factory("while"), new WhileStatementParser());
		put(JackKeyword.factory("do"), new DoStatementParser());
		put(JackKeyword.factory("return"), new ReturnStatementParser());

	}};

	public static JackStatement parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {


		JackToken token = jackTokenFeeder.peekNext();

		JackStatementParselet parselet = parselets.get(token);

		if (parselet == null) {
			return null;
		} else {
			return parselet.parseStatement(jackTokenFeeder);
		}
	}

}
