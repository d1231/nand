package jack.analyzer;

import jack.analyzer.jack_class.JackClass;
import jack.analyzer.tokenizer.JackTokenizer;
import jack.analyzer.utils.JackUtils;

import java.io.File;

/**
 * Created by danny on 10/04/15.
 */
public class JackAnalyzer {

	private final File file;

	private JackTokenizer tokenizer;

	public JackAnalyzer(File file) {

		this.file = file;


	}

	public void analyzer() throws Exception {

		tokenizer = new JackTokenizer(file);
		JackComplicationEngine complicationEngine = new JackComplicationEngine(tokenizer);

		JackClass jackClass = complicationEngine.compile();

		try (JackXMLWriter writer = new JackXMLWriter(getOutputFile())) {

			jackClass.writeXML(writer);
		}


	}

	private File getOutputFile() {

		// TODO better
		// case of file like idiot/.jack/file.jack

		return new File(file.getAbsolutePath().replace(JackUtils.JACK_SUFFIX, ".xml"));
	}


}
