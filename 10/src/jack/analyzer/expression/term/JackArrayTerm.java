package jack.analyzer.expression.term;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class JackArrayTerm extends JackTerm {

	private final static JackSymbol openingSquareBracket = JackSymbol.factory('[');

	private final static JackSymbol closingSquareBracket = JackSymbol.factory(']');

	private final JackToken name;

	private final JackExpression expression;

	public JackArrayTerm(JackToken name, JackExpression expression) {

		this.name = name;
		this.expression = expression;
	}

	@Override
	protected void writeTermContent(JackXMLWriter writer) throws IOException {

		name.writeXML(writer);
		openingSquareBracket.writeXML(writer);
		expression.writeXML(writer);
		closingSquareBracket.writeXML(writer);
	}

	@Override
	public String toString() {

		return "JackArrayTerm{" +
				"name=" + name +
				", expression=" + expression +
				'}';
	}


}
