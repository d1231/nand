package jack.analyzer.expression.term;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class UnaryJackTerm extends JackTerm {

	private final JackTerm unaryTerm;

	private final JackToken op;

	public UnaryJackTerm(JackToken op, JackTerm unaryTerm) {

		this.op = op;
		this.unaryTerm = unaryTerm;

	}

	@Override
	protected void writeTermContent(JackXMLWriter writer) throws IOException {

		op.writeXML(writer);
		unaryTerm.writeXML(writer);
	}

	@Override
	public String toString() {

		return "UnaryJackTerm{" +
				"unaryTerm=" + unaryTerm +
				", op=" + op +
				'}';
	}
}
