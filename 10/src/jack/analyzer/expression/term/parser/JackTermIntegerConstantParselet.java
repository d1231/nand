package jack.analyzer.expression.term.parser;

import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.JackTermWrapper;
import jack.analyzer.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermIntegerConstantParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException {

		return new JackTermWrapper(jackTokenFeeder.consumeNext());
	}
}
