package jack.analyzer.expression.term.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.expression.term.JackArrayTerm;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class JackArrayTermParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		JackToken name = jackTokenFeeder.consumeNext();

		// consume '['
		if (!JackUtils.isOpeningSquareBracket(jackTokenFeeder.consumeNext())) {
			throw new JackTermParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		// consume ']'
		if (!JackUtils.isClosingSquareBracket(jackTokenFeeder.consumeNext())) {
			throw new JackTermParserException();
		}

		return new JackArrayTerm(name, expression);
	}

}
