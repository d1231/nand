package jack.analyzer.expression.term.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermSymbolParselet implements JackTermParselet {

	private final static HashMap<String, JackTermParselet> parselets = new HashMap<String, JackTermParselet>() {{

		put("~", new UnaryJackTermParselet());
		put("-", new UnaryJackTermParselet());
		put("(", new ParenthesesJackTermParselet());

	}};

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		JackToken token = jackTokenFeeder.peekNext();

		JackTermParselet parselet = parselets.get(token.getValue());

		if (parselet == null) {
			return null;
		} else {
			return parselet.parse(jackTokenFeeder);
		}
	}

}
