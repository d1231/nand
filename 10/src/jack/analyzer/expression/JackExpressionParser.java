package jack.analyzer.expression;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.JackTermParser;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.tokenizer.token.JackTokenType;
import jack.analyzer.utils.Tuple;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danny on 18/04/15.
 */
public class JackExpressionParser {

	private final static Set<String> operators = new HashSet<String>() {{

		add("+");
		add("-");
		add("*");
		add("/");
		add("&amp;");
		add("|");
		add("&gt;");
		add("&lt;");
		add("=");

	}};

	public static JackExpression parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		JackExpressionBuilder builder = new JackExpressionBuilder();
		JackTerm term = JackTermParser.parse(jackTokenFeeder);

		if (term == null) {
			return null;
		}

		builder.setTerm(term);

		while (isOperator(jackTokenFeeder.peekNext())) {
			builder.addTerm(jackTokenFeeder.consumeNext(), JackTermParser.parse(jackTokenFeeder));
		}

		return builder.build();
	}

	private static boolean isOperator(JackToken jackToken) {

		return jackToken.getTokenType().equals(JackTokenType.SYMBOL) && operators.contains(jackToken.getValue());

	}

	private static class JackExpressionBuilder {

		private JackTerm term;

		private ArrayList<Tuple<JackToken, JackTerm>> additionalTerms = new ArrayList<>();

		public void setTerm(JackTerm term) {

			this.term = term;
		}

		public void addTerm(JackToken op, JackTerm term) {

			additionalTerms.add(new Tuple<>(op, term));

		}

		public JackExpression build() {

			return new JackExpression(term, additionalTerms);
		}
	}
}
