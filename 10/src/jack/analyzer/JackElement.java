package jack.analyzer;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public interface JackElement {

	public void writeXML(JackXMLWriter writer) throws IOException;
}
