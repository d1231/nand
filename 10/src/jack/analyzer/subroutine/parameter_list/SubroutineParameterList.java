package jack.analyzer.subroutine.parameter_list;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.utils.Tuple;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineParameterList implements JackElement {

	private final static JackToken separator = JackSymbol.factory(',');

	private static final String PARAMETER_LIST = "parameterList";

	private final ArrayList<Tuple<JackToken, JackToken>> params;

	public SubroutineParameterList(ArrayList<Tuple<JackToken, JackToken>> params) {

		this.params = params;

	}

	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(PARAMETER_LIST);

		if (params.size() > 0) {
			Tuple<JackToken, JackToken> tokenTuple = params.get(0);
			tokenTuple.x.writeXML(writer);
			tokenTuple.y.writeXML(writer);

			for (int i = 1; i < params.size(); i++) {
				tokenTuple = params.get(i);
				separator.writeXML(writer);
				tokenTuple.x.writeXML(writer);
				tokenTuple.y.writeXML(writer);
			}
		}

		writer.endTag(PARAMETER_LIST);

	}

	@Override
	public String toString() {

		return "SubroutineParameterList{" +
				"params=" + params +
				'}';
	}
}
