package jack.analyzer.subroutine;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.subroutine.body.SubroutineBody;
import jack.analyzer.subroutine.parameter_list.SubroutineParameterList;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineDeceleration implements JackElement {

	public static final String SUBROUTINE_DEC = "subroutineDec";

	private static final JackToken openingBracket = JackSymbol.factory('(');

	private static final JackToken closingBracket = JackSymbol.factory(')');

	private final JackToken type;

	private final JackToken returnType;

	private final JackToken subroutineName;

	private final SubroutineParameterList parameterList;

	private final SubroutineBody subroutineBody;

	public SubroutineDeceleration(JackToken type, JackToken returnType, JackToken subroutineName,
			SubroutineParameterList parameterList, SubroutineBody subroutineBody) {

		this.type = type;
		this.returnType = returnType;
		this.subroutineName = subroutineName;
		this.parameterList = parameterList;
		this.subroutineBody = subroutineBody;
	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(SUBROUTINE_DEC);

		type.writeXML(writer);
		returnType.writeXML(writer);
		subroutineName.writeXML(writer);
		openingBracket.writeXML(writer);
		parameterList.writeXML(writer);
		closingBracket.writeXML(writer);
		subroutineBody.writeXML(writer);

		writer.endTag(SUBROUTINE_DEC);

	}

	@Override
	public String toString() {

		return "SubroutineDeceleration{" +
				"type=" + type +
				", returnType=" + returnType +
				", subroutineName=" + subroutineName +
				", parameterList=" + parameterList +
				", subroutineBody=" + subroutineBody +
				'}';
	}
}
