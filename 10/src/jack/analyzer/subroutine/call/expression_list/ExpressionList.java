package jack.analyzer.subroutine.call.expression_list;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 19/04/15.
 */
public class ExpressionList implements JackElement {

	public static final String EXPRESSION_LIST = "expressionList";

	private final static JackToken separator = JackSymbol.factory(',');

	private final ArrayList<JackExpression> expressions;

	public ExpressionList(ArrayList<JackExpression> expressions) {

		this.expressions = expressions;

	}

	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(EXPRESSION_LIST);

		final int size = expressions.size();
		if (size > 0) {

			int i;
			for (i = 0; i < size - 1; i++) {
				expressions.get(i).writeXML(writer);
				separator.writeXML(writer);
			}

			expressions.get(i).writeXML(writer);

		}

		writer.endTag(EXPRESSION_LIST);

	}

	@Override
	public String toString() {

		return "ExpressionList{" +
				"expressions=" + expressions +
				'}';
	}
}
