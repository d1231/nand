package jack.analyzer.tokenizer;

import jack.analyzer.JackTokenFeeder;
import jack.analyzer.tokenizer.token.*;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by danny on 10/04/15.
 */
public class JackTokenizer implements AutoCloseable, JackTokenFeeder {


	final static Charset ENCODING = StandardCharsets.UTF_8;

	public static final int TAB_REPLACEMENT = 21;

	private final StreamTokenizer tokenizer;

	private final Reader reader;

	private int tokenizerType;

	private JackToken savedToken;

	public JackTokenizer(File file) throws IOException {

		Path path = Paths.get(file.getAbsolutePath());
		reader = new ReaderForTest(Files.newBufferedReader(path, ENCODING));
		tokenizer = new StreamTokenizer(reader);

		// remove comments
		tokenizer.slashSlashComments(true);
		tokenizer.slashStarComments(true);

		// ignore end of line
		tokenizer.eolIsSignificant(false);

		tokenizer.quoteChar('"');
		tokenizer.wordChars('_', '_'); // add _ to words

		for (char c : JackSymbol.getAllSymbols()) {
			tokenizer.ordinaryChar(c);
		}


	}

	@Override
	public void close() throws IOException {

		reader.close();
	}

	@Override
	public JackToken peekNextNext() throws IOException, JackTokenizerException {

		JackToken lSavedToken = consumeNext();

		JackToken retVal = peekNext();

		savedToken = lSavedToken;

		return retVal;

	}

	@Override
	public JackToken consumeNext() throws IOException, JackTokenizerException {

		JackToken retVal;
		if (savedToken != null) {
			retVal = savedToken;
			savedToken = null;
		} else {
			if (moveToNext()) {
				retVal = get();
			} else {
				retVal = null;
			}
		}
		return retVal;

	}

	@Override
	public JackToken peekNext() throws IOException, JackTokenizerException {

		JackToken retVal = consumeNext();
		tokenizer.pushBack();
		return retVal;
	}

	public boolean moveToNext() throws IOException {

		return (tokenizerType = tokenizer.nextToken()) != StreamTokenizer.TT_EOF;

	}

	private JackToken get() throws JackTokenizerException, IOException {

		final String sval = tokenizer.sval;

		JackToken retVal;
		switch (tokenizerType) {
			case StreamTokenizer.TT_NUMBER:
				retVal = new JackIntegerConstant((int) tokenizer.nval);
				break;
			case StreamTokenizer.TT_WORD:
				retVal = JackKeyword.factory(sval);
				if (retVal == null) {
					retVal = new JackIdentifier(sval);
				}
				break;
			case '"':
				retVal = new JackStringConstant(sval);
				break;
			default:

				retVal = JackSymbol.factory((char) tokenizer.ttype);
				if (retVal == null) {
					throw new JackTokenizerException("Unknown symbol");
				}
				break;
		}

		return retVal;

	}

	private String getStringConstant() throws IOException {

		StringBuilder sb = new StringBuilder();

		moveToNext();
		while (tokenizerType != '"') {
			if (tokenizer.sval != null) {
				sb.append(tokenizer.sval);
			} else {
				sb.append(tokenizer.nval);
			}

			moveToNext();
		}

		return sb.toString();
	}

	/**
	 * Stupid class
	 */
	private class ReaderForTest extends Reader {

		private final BufferedReader reader;

		public ReaderForTest(BufferedReader reader) {

			this.reader = reader;
		}

		@Override
		public int read() throws IOException {

			final int read = super.read();

			if (read == '\t') {
				return TAB_REPLACEMENT;
			} else {
				return read;
			}
		}

		@Override
		public int read(char[] cbuf, int off, int len) throws IOException {


			return reader.read(cbuf, off, len);
		}

		@Override
		public void close() throws IOException {

			reader.close();
		}
	}

}
