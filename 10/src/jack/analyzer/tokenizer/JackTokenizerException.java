package jack.analyzer.tokenizer;

/**
 * Created by danny on 13/04/15.
 */
public class JackTokenizerException extends Exception {

	public JackTokenizerException(String s) {

		super(s);

	}
}
