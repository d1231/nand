package jack.analyzer.tokenizer.token;

import jack.analyzer.JackXMLWriter;

import java.io.IOException;

/**
 * Created by danny on 17/04/15.
 */
public class JackIntegerConstant extends JackToken {

	public static final String INTEGER_CONSTANT = "integerConstant";

	public JackIntegerConstant(int value) {

		super(JackTokenType.INTEGER_CONSTANT, String.valueOf(value));
	}

	@Override
	public void writeXML(JackXMLWriter jackXMLWriter) throws IOException {

		jackXMLWriter.writeTag(INTEGER_CONSTANT, getValue());

	}
}
