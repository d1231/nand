package jack.analyzer.tokenizer.token;

import jack.analyzer.JackElement;

/**
 * Created by danny on 13/04/15.
 */
public abstract class JackToken implements JackElement {

	private final JackTokenType jackTokenType;

	private final String value;

	public JackToken(JackTokenType jackTokenType, String value) {

		this.jackTokenType = jackTokenType;
		this.value = value;
	}

	public String getValue() {

		return value;
	}

	public JackTokenType getTokenType() {

		return jackTokenType;
	}

	@Override
	public int hashCode() {

		int result = jackTokenType != null ? jackTokenType.hashCode() : 0;
		result = 31 * result + (value != null ? value.hashCode() : 0);
		return result;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		JackToken token = (JackToken) o;

		if (jackTokenType != token.jackTokenType)
			return false;
		if (value != null ? !value.equals(token.value) : token.value != null)
			return false;

		return true;
	}

	@Override
	public String toString() {

		return "JackToken{" +
				"jackTokenType=" + jackTokenType +
				", value='" + value + '\'' +
				'}';
	}
}
