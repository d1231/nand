package jack.analyzer.tokenizer.token;

import jack.analyzer.JackXMLWriter;

import java.io.IOException;

/**
 * Created by danny on 17/04/15.
 */
public class JackIdentifier extends JackToken {

	public static final String IDENTIFIER = "identifier";

	public JackIdentifier(String value) {

		super(JackTokenType.IDENTIFIER, value);
	}

	@Override
	public void writeXML(JackXMLWriter jackXMLWriter) throws IOException {

		jackXMLWriter.writeTag(IDENTIFIER, getValue());

	}
}
