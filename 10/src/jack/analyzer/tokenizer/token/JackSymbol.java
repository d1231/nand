package jack.analyzer.tokenizer.token;

import jack.analyzer.JackXMLWriter;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 13/04/15.
 */
public class JackSymbol extends JackToken {

	private final static HashMap<Character, JackSymbol> symbolMap = new HashMap<Character, JackSymbol>() {{

		put('{', new JackSymbol("{"));
		put('}', new JackSymbol("}"));
		put('(', new JackSymbol("("));
		put(')', new JackSymbol(")"));
		put('[', new JackSymbol("["));
		put(']', new JackSymbol("]"));
		put('.', new JackSymbol("."));
		put(',', new JackSymbol(","));
		put(';', new JackSymbol(";"));
		put('+', new JackSymbol("+"));
		put('-', new JackSymbol("-"));
		put('*', new JackSymbol("*"));
		put('/', new JackSymbol("/"));
		put('&', new JackSymbol("&amp;"));
		put('|', new JackSymbol("|"));
		put('<', new JackSymbol("&lt;"));
		put('>', new JackSymbol("&gt;"));
		put('=', new JackSymbol("="));
		put('~', new JackSymbol("~"));

	}};

	private static final String SYMBOL = "symbol";

	private JackSymbol(String value) {

		super(JackTokenType.SYMBOL, value);
	}

	public static Iterable<? extends Character> getAllSymbols() {

		return symbolMap.keySet();
	}

	public static JackSymbol factory(Character value) {

		return symbolMap.get(value);

	}

	@Override
	public void writeXML(JackXMLWriter jackXMLWriter) throws IOException {

		jackXMLWriter.writeTag(SYMBOL, getValue());
	}
}
