package jack.analyzer.tokenizer.token;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.JackTokenizer;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by danny on 17/04/15.
 */
public class JackStringConstant extends JackToken {

	public static final String STRING_CONSTANT = "stringConstant";

	private static final String SPECIAL_CHARS_REGEX = "\\\\(?:\\w)";

	private static final Pattern p = Pattern.compile(SPECIAL_CHARS_REGEX);

	public JackStringConstant(String value) {

		super(JackTokenType.STRING_CONSTANT, value);
	}

	@Override
	public void writeXML(JackXMLWriter jackXMLWriter) throws IOException {


		String value = getValue();


		value = value.replaceAll("\t", "\\\\t");
		value = value.replace((char) JackTokenizer.TAB_REPLACEMENT, '\t');
		value = value.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quote;");
		value = value.replaceAll("\r", "\\\\r");
		value = value.replaceAll("\n", "\\\\n");

		jackXMLWriter.writeTag(STRING_CONSTANT, value);

	}
}
