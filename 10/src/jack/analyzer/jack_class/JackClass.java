package jack.analyzer.jack_class;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.subroutine.SubroutineDeceleration;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.variable_deceleration.ClassVariableDeceleration;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 19/04/15.
 */
public class JackClass implements JackElement {

	private static final String CLASS = "class";

	private static final JackKeyword CLASS_KEYWORD = JackKeyword.factory("class");

	private final static JackSymbol OPENING_CURLY_BRACKET = JackSymbol.factory('{');

	private final static JackSymbol CLOSING_CURLY_BRACKET = JackSymbol.factory('}');

	private final JackToken className;

	private final ArrayList<ClassVariableDeceleration> classVariableDecelerations;

	private final ArrayList<SubroutineDeceleration> subroutineDecelerations;

	public JackClass(JackToken className, ArrayList<ClassVariableDeceleration> classVariableDecelerations,
			ArrayList<SubroutineDeceleration> subroutineDecelerations) {

		this.className = className;
		this.classVariableDecelerations = classVariableDecelerations;
		this.subroutineDecelerations = subroutineDecelerations;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(CLASS);

		CLASS_KEYWORD.writeXML(writer);
		className.writeXML(writer);
		OPENING_CURLY_BRACKET.writeXML(writer);

		for (ClassVariableDeceleration classVariableDeceleration : classVariableDecelerations) {
			classVariableDeceleration.writeXML(writer);
		}

		for (SubroutineDeceleration subroutineDeceleration : subroutineDecelerations) {
			subroutineDeceleration.writeXML(writer);
		}

		CLOSING_CURLY_BRACKET.writeXML(writer);

		writer.endTag(CLASS);
	}
}
