package jack.analyzer.statements.statement;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class LetStatement implements JackStatement {

	public static final String LET_STATEMENT = "letStatement";

	private final static JackKeyword letKeyword = JackKeyword.factory("let");

	private final static JackSymbol openingSquareBracket = JackSymbol.factory('[');

	private final static JackSymbol closingSquareBracket = JackSymbol.factory(']');

	private final static JackSymbol equalSign = JackSymbol.factory('=');

	private final static JackSymbol endToken = JackSymbol.factory(';');

	private final JackToken name;

	private final JackExpression arrayExpression;

	private final JackExpression expression;

	public LetStatement(JackToken name, JackExpression arrayExpression, JackExpression expression) {

		this.name = name;
		this.arrayExpression = arrayExpression;
		this.expression = expression;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(LET_STATEMENT);

		letKeyword.writeXML(writer);
		name.writeXML(writer);

		if (arrayExpression != null) {
			openingSquareBracket.writeXML(writer);
			arrayExpression.writeXML(writer);
			closingSquareBracket.writeXML(writer);
		}

		equalSign.writeXML(writer);

		expression.writeXML(writer);

		endToken.writeXML(writer);

		writer.endTag(LET_STATEMENT);

	}

	@Override
	public String toString() {

		return "LetStatement{" +
				"name=" + name +
				", arrayExpression=" + arrayExpression +
				", expression=" + expression +
				'}';
	}
}
