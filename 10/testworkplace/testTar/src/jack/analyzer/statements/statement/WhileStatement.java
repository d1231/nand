package jack.analyzer.statements.statement;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatements;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackSymbol;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class WhileStatement implements JackStatement {

	private static final String WHILE_STATEMENT = "whileStatement";

	private final static JackSymbol openingBracket = JackSymbol.factory('(');

	private final static JackSymbol closingBracket = JackSymbol.factory(')');

	private final static JackSymbol openingCurlyBracket = JackSymbol.factory('{');

	private final static JackSymbol closingCurlyBracket = JackSymbol.factory('}');

	private static final JackKeyword WHILE = JackKeyword.factory("while");

	private final JackExpression expression;

	private final JackStatements statements;

	public WhileStatement(JackExpression expression, JackStatements statements) {

		this.expression = expression;

		this.statements = statements;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(WHILE_STATEMENT);

		WHILE.writeXML(writer);

		openingBracket.writeXML(writer);

		expression.writeXML(writer);

		closingBracket.writeXML(writer);

		openingCurlyBracket.writeXML(writer);

		statements.writeXML(writer);

		closingCurlyBracket.writeXML(writer);

		writer.endTag(WHILE_STATEMENT);
	}

	@Override
	public String toString() {

		return "WhileStatement{" +
				"expression=" + expression +
				", statements=" + statements +
				'}';
	}
}
