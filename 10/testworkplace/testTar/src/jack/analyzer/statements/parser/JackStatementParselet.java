package jack.analyzer.statements.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public interface JackStatementParselet {

	JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException;

}
