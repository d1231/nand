package jack.analyzer.statements.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatementParserException;
import jack.analyzer.statements.statement.ReturnStatement;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class ReturnStatementParser implements JackStatementParselet {

	private static final JackKeyword RETURN = JackKeyword.factory("return");

	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!RETURN.equals(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		if (!JackUtils.isEndToken(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		return new ReturnStatement(expression);

	}
}
