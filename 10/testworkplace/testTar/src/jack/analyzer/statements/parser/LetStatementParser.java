package jack.analyzer.statements.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatementParserException;
import jack.analyzer.statements.statement.LetStatement;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class LetStatementParser implements JackStatementParselet {

	private static final JackToken letToken = JackKeyword.factory("let");

	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		LetStatementBuilder builder = new LetStatementBuilder();

		JackToken token = jackTokenFeeder.consumeNext();

		if (!token.equals(letToken)) {
			throw new JackStatementParserException();
		}

		builder.setName(jackTokenFeeder.consumeNext());

		// parse [ expression ] if there is
		if (JackUtils.isOpeningSquareBracket(jackTokenFeeder.peekNext())) {

			jackTokenFeeder.consumeNext();

			builder.setArrayExpression(JackExpressionParser.parse(jackTokenFeeder));

			if (!JackUtils.isClosingSquareBracket(jackTokenFeeder.consumeNext())) {
				throw new JackStatementParserException();
			}

		}

		if (!JackUtils.isEqualSign(jackTokenFeeder.consumeNext())) {

			throw new JackStatementParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);
		builder.setExpression(expression);

		if (!JackUtils.isEndToken(jackTokenFeeder.consumeNext())) {

			throw new JackStatementParserException();
		}

		return builder.build();
	}

	private static class LetStatementBuilder {

		private JackToken name;

		private JackExpression arrayExpression = null;

		private JackExpression expression;

		public void setName(JackToken name) {

			this.name = name;
		}

		public void setArrayExpression(JackExpression arrayExpression) {

			this.arrayExpression = arrayExpression;
		}

		public void setExpression(JackExpression expression) {

			this.expression = expression;
		}

		public JackStatement build() {

			return new LetStatement(name, arrayExpression, expression);
		}
	}
}
