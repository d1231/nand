package jack.analyzer.statements.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatementParserException;
import jack.analyzer.statements.JackStatements;
import jack.analyzer.statements.JackStatementsParser;
import jack.analyzer.statements.statement.WhileStatement;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class WhileStatementParser implements JackStatementParselet {

	private static final JackKeyword WHILE = JackKeyword.factory("while");

	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!WHILE.equals(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		if (!JackUtils.isOpeningBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		if (!JackUtils.isOpeningCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		// statements parser
		JackStatements statements = JackStatementsParser.parse(jackTokenFeeder);

		if (!JackUtils.isClosingCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}


		return new WhileStatement(expression, statements);
	}
}
