package jack.analyzer.statements.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.statements.JackStatement;
import jack.analyzer.statements.JackStatementParserException;
import jack.analyzer.statements.JackStatements;
import jack.analyzer.statements.JackStatementsParser;
import jack.analyzer.statements.statement.IfStatement;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class IfStatementParser implements JackStatementParselet {

	private static final JackKeyword IF = JackKeyword.factory("if");

	private static final JackKeyword ELSE = JackKeyword.factory("else");

	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!IF.equals(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		if (!JackUtils.isOpeningBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		if (!JackUtils.isOpeningCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		// statements parser
		JackStatements statements = JackStatementsParser.parse(jackTokenFeeder);

		if (!JackUtils.isClosingCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		JackStatements elseStatements = null;
		if (ELSE.equals(jackTokenFeeder.peekNext())) {

			// consume else
			jackTokenFeeder.consumeNext();

			// consume '{'
			if (!JackUtils.isOpeningCurlyBracket(jackTokenFeeder.consumeNext())) {
				throw new JackStatementParserException();
			}

			elseStatements = JackStatementsParser.parse(jackTokenFeeder);

			// consume '}'
			if (!JackUtils.isClosingCurlyBracket(jackTokenFeeder.consumeNext())) {
				throw new JackStatementParserException();
			}

		}

		return new IfStatement(expression, statements, elseStatements);
	}
}
