package jack.analyzer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

/**
 * Created by danny on 13/04/15.
 */
public class JackXMLWriter implements AutoCloseable {

	public static final String TAG_FORMAT = "<%s> %s </%s>";

	public static final String TAG_START = "<%s>";

	private static final String TAG_END = "</%s>";

	private final BufferedWriter bufferedWriter;

	private int level = 0;

	public JackXMLWriter(File file) throws IOException {

		this.bufferedWriter = new BufferedWriter(new FileWriter(file));
	}

	@Override
	public void close() throws Exception {

		bufferedWriter.close();
	}

	public void writeTag(String tag, String value) throws IOException {

		writeLevel();
		if (Objects.equals(value, "") || value == null) {
			value = "\\n";
		}
		bufferedWriter.write(String.format(TAG_FORMAT, tag, value, tag));
		bufferedWriter.newLine();

	}

	private void writeLevel() throws IOException {

		for (int i = 0; i < level; i++) {
			bufferedWriter.write('\t');
		}

	}

	public void startTag(String tag) throws IOException {

		writeLevel();
		bufferedWriter.write(String.format(TAG_START, tag));
		bufferedWriter.newLine();
		level++;

	}

	public void endTag(String tag) throws IOException {

		level--;
		writeLevel();
		bufferedWriter.write(String.format(TAG_END, tag));
		bufferedWriter.newLine();

	}
}
