package jack.analyzer.expression.term.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.ParenthesesJackTerm;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class ParenthesesJackTermParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		jackTokenFeeder.consumeNext(); // consume '('

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		// consume ')'
		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new JackTermParserException();
		}

		return new ParenthesesJackTerm(expression);
	}
}
