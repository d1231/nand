package jack.analyzer.expression.term;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.parser.*;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.tokenizer.token.JackTokenType;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermParser {

	private final static HashMap<JackTokenType, JackTermParselet> parselets = new HashMap<JackTokenType,
			JackTermParselet>() {{

		put(JackTokenType.IDENTIFIER, new JackTermIdentifierParselet());
		put(JackTokenType.SYMBOL, new JackTermSymbolParselet());
		put(JackTokenType.INTEGER_CONSTANT, new JackTermIntegerConstantParselet());
		put(JackTokenType.STRING_CONSTANT, new JackTermStringConstantParselet());
		put(JackTokenType.KEYWORD, new JackTermKeywordParselet());

	}};

	public static JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		JackToken token = jackTokenFeeder.peekNext();

		return parselets.get(token.getTokenType()).parse(jackTokenFeeder);

	}
}
