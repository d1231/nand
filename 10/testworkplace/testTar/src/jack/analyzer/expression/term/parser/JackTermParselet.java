package jack.analyzer.expression.term.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public interface JackTermParselet {

	JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException;
}
