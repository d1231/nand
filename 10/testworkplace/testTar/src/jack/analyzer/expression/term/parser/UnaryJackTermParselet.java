package jack.analyzer.expression.term.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.JackTermParser;
import jack.analyzer.expression.term.UnaryJackTerm;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class UnaryJackTermParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		JackToken op = jackTokenFeeder.consumeNext();
		JackTerm unaryTerm = JackTermParser.parse(jackTokenFeeder);

		return new UnaryJackTerm(op, unaryTerm);
	}
}
