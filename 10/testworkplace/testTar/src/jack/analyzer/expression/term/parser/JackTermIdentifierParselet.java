package jack.analyzer.expression.term.parser;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.JackTermWrapper;
import jack.analyzer.subroutine.call.SubRoutineCallParselet;
import jack.analyzer.subroutine.call.SubroutineInvokeParselet;
import jack.analyzer.tokenizer.JackTokenizerException;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermIdentifierParselet implements JackTermParselet {

	private final static HashMap<String, JackTermParselet> parselets = new HashMap<String, JackTermParselet>() {{

		put("[", new JackArrayTermParselet());
		put(".", new SubroutineInvokeParselet(true));
		put("(", new SubRoutineCallParselet(true));

	}};


	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		JackTermParselet parselet = parselets.get(jackTokenFeeder.peekNextNext().getValue());

		if (parselet == null) {
			return new JackTermWrapper(jackTokenFeeder.consumeNext());
		} else {
			return parselet.parse(jackTokenFeeder);
		}

	}

}
