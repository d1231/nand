package jack.analyzer.expression.term;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class ParenthesesJackTerm extends JackTerm {

	private final static JackToken openingBracket = JackSymbol.factory('(');

	private final static JackToken closingBracket = JackSymbol.factory(')');

	private final JackExpression expression;

	public ParenthesesJackTerm(JackExpression expression) {

		this.expression = expression;
	}

	@Override
	protected void writeTermContent(JackXMLWriter writer) throws IOException {

		openingBracket.writeXML(writer);

		expression.writeXML(writer);

		closingBracket.writeXML(writer);

	}

	@Override
	public String toString() {

		return "ParenthesesJackTerm{" +
				"expression=" + expression +
				'}';
	}
}
