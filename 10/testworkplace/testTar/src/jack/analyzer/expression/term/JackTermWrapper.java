package jack.analyzer.expression.term;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermWrapper extends JackTerm {

	private final JackToken wrapped;

	public JackTermWrapper(JackToken jackToken) {

		this.wrapped = jackToken;
	}

	@Override
	protected void writeTermContent(JackXMLWriter writer) throws IOException {

		wrapped.writeXML(writer);

	}

	@Override
	public String toString() {

		return "JackTermWrapper{" +
				"wrapped=" + wrapped +
				'}';
	}
}
