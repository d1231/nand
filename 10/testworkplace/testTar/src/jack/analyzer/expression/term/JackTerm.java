package jack.analyzer.expression.term;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public abstract class JackTerm implements JackElement {

	private static final String TERM = "term";

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(TERM);

		writeTermContent(writer);

		writer.endTag(TERM);

	}

	protected abstract void writeTermContent(JackXMLWriter writer) throws IOException;
}
