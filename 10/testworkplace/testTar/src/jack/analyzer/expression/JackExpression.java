package jack.analyzer.expression;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.utils.Tuple;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 17/04/15.
 */
public class JackExpression implements JackElement {

	public static final String EXPRESSION = "expression";

	private final JackTerm term;

	private final ArrayList<Tuple<JackToken, JackTerm>> additionalTerms;

	public JackExpression(JackTerm term, ArrayList<Tuple<JackToken, JackTerm>> additionalTerms) {

		this.term = term;
		this.additionalTerms = additionalTerms;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(EXPRESSION);

		term.writeXML(writer);

		for (Tuple<JackToken, JackTerm> additionalTerm : additionalTerms) {

			additionalTerm.x.writeXML(writer);
			additionalTerm.y.writeXML(writer);

		}

		writer.endTag(EXPRESSION);

	}

	@Override
	public String toString() {

		return "JackExpression{" +
				"term=" + term +
				", additionalTerms=" + additionalTerms +
				'}';
	}
}
