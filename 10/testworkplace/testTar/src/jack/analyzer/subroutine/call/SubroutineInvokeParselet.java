package jack.analyzer.subroutine.call;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.expression.term.parser.JackTermParselet;
import jack.analyzer.subroutine.call.expression_list.ExpressionList;
import jack.analyzer.subroutine.call.expression_list.ExpressionListParselet;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class SubroutineInvokeParselet implements JackTermParselet {

	private boolean isPartOfExpression;

	public SubroutineInvokeParselet(boolean isPartOfExpression) {

		this.isPartOfExpression = isPartOfExpression;
	}

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {


		JackToken inkovee = jackTokenFeeder.consumeNext();

		if (!JackUtils.isPoint(jackTokenFeeder.consumeNext())) {
			throw new JackSubroutineCallParseletException();
		}

		JackToken name = jackTokenFeeder.consumeNext();

		if (!JackUtils.isOpeningBracket(jackTokenFeeder.consumeNext())) {
			throw new JackSubroutineCallParseletException();
		}

		ExpressionList expressionList = ExpressionListParselet.parse(jackTokenFeeder);

		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new JackSubroutineCallParseletException();
		}


		return new SubroutineInvoke(inkovee, name, expressionList, isPartOfExpression);
	}
}
