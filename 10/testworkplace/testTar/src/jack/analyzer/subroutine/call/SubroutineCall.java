package jack.analyzer.subroutine.call;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.expression.term.JackTerm;
import jack.analyzer.subroutine.call.expression_list.ExpressionList;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class SubroutineCall extends JackTerm {

	private final static JackToken openingBracket = JackSymbol.factory('(');

	private final static JackToken closingBracket = JackSymbol.factory(')');

	private final JackToken subroutineName;

	private final ExpressionList expressionList;

	private final boolean isPartOfExpression;

	public SubroutineCall(JackToken name, ExpressionList expressionList, boolean isPartOfExpression) {

		subroutineName = name;
		this.expressionList = expressionList;
		this.isPartOfExpression = isPartOfExpression;
	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		if (isPartOfExpression) {
			super.writeXML(writer);
		} else {
			writeTermContent(writer);
		}
	}

	@Override
	protected void writeTermContent(JackXMLWriter writer) throws IOException {

		subroutineName.writeXML(writer);
		openingBracket.writeXML(writer);
		expressionList.writeXML(writer);
		closingBracket.writeXML(writer);

	}

	@Override
	public String toString() {

		return "SubroutineCall{" +
				"subroutineName=" + subroutineName +
				", expressionList=" + expressionList +
				", isPartOfExpression=" + isPartOfExpression +
				'}';
	}
}
