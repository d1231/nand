package jack.analyzer.subroutine.call;

import jack.analyzer.JackXMLWriter;
import jack.analyzer.subroutine.call.expression_list.ExpressionList;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class SubroutineInvoke extends SubroutineCall {

	private final static JackSymbol point = JackSymbol.factory('.');

	private final JackToken invokee;

	public SubroutineInvoke(JackToken inkovee, JackToken name, ExpressionList expressionList, boolean
			isPartOfExpression) {

		super(name, expressionList, isPartOfExpression);
		this.invokee = inkovee;
	}

	@Override
	protected void writeTermContent(JackXMLWriter writer) throws IOException {

		invokee.writeXML(writer);
		point.writeXML(writer);

		super.writeTermContent(writer);
	}

	@Override
	public String toString() {

		return "SubroutineInvoke{" +
				"invokee=" + invokee +
				"}, " + super.toString();
	}
}
