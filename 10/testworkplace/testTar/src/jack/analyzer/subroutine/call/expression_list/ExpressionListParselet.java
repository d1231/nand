package jack.analyzer.subroutine.call.expression_list;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.expression.JackExpression;
import jack.analyzer.expression.JackExpressionParser;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 19/04/15.
 */
public class ExpressionListParselet {

	public static ExpressionList parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		ExpressionListParseletBuilder builder = new ExpressionListParseletBuilder();


		JackExpression expression;
		if ((expression = JackExpressionParser.parse(jackTokenFeeder)) != null) {
			builder.addExpression(expression);

			while (JackUtils.isSeparator(jackTokenFeeder.peekNext())) {
				jackTokenFeeder.consumeNext();
				expression = JackExpressionParser.parse(jackTokenFeeder);
				builder.addExpression(expression);
			}
		}

		return builder.build();
	}

	private static class ExpressionListParseletBuilder {

		private ArrayList<JackExpression> expressions = new ArrayList<>();

		public void addExpression(JackExpression expression) {

			expressions.add(expression);

		}

		public ExpressionList build() {

			return new ExpressionList(expressions);
		}
	}
}
