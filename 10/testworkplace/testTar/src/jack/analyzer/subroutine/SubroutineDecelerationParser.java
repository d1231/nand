package jack.analyzer.subroutine;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.subroutine.body.SubroutineBody;
import jack.analyzer.subroutine.body.SubroutineBodyParser;
import jack.analyzer.subroutine.parameter_list.SubroutineParameterList;
import jack.analyzer.subroutine.parameter_list.SubroutineParameterListParser;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackKeyword;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.tokenizer.token.JackTokenType;
import jack.analyzer.utils.JackUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineDecelerationParser {

	public static final String VOID = "void";

	private final static Set<String> types = new HashSet<String>() {{
		add("constructor");
		add("function");
		add("method");
	}};

	public static SubroutineDeceleration parse(JackTokenFeeder jackTokenFeeder) throws IOException,
			JackTokenizerException, JackParserException {


		if (!isSubroutineType(jackTokenFeeder.peekNext())) {
			return null;
		}

		JackToken type = jackTokenFeeder.consumeNext();

		JackToken returnType = jackTokenFeeder.consumeNext();

		if (!JackUtils.isType(returnType) && !JackKeyword.factory(VOID).equals(returnType)) {
			throw new SubroutineParserException();
		}

		JackToken name = jackTokenFeeder.consumeNext();

		if (!JackUtils.isValidName(name)) {
			throw new SubroutineParserException();
		}

		if (!JackUtils.isOpeningBracket(jackTokenFeeder.consumeNext())) {
			throw new SubroutineParserException();
		}

		SubroutineParameterList parameterList = SubroutineParameterListParser.parse(jackTokenFeeder);


		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new SubroutineParserException();
		}

		SubroutineBody subroutineBody = SubroutineBodyParser.parse(jackTokenFeeder);

		return new SubroutineDeceleration(type, returnType, name, parameterList, subroutineBody);


	}

	private static boolean isSubroutineType(JackToken jackToken) {

		return jackToken.getTokenType().equals(JackTokenType.KEYWORD) && types.contains(jackToken.getValue());
	}


}
