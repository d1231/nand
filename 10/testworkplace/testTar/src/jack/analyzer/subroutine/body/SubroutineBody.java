package jack.analyzer.subroutine.body;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.statements.JackStatements;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.variable_deceleration.SubroutineVariableDeceleration;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineBody implements JackElement {

	public static final String SUBROUTINE_BODY = "subroutineBody";

	private final static JackSymbol openingBracket = JackSymbol.factory('{');

	private final static JackSymbol closingBracket = JackSymbol.factory('}');

	private final ArrayList<SubroutineVariableDeceleration> subRoutineVarDec;

	private final JackStatements statements;

	public SubroutineBody(ArrayList<SubroutineVariableDeceleration> subRoutineVarDec, JackStatements statements) {

		this.subRoutineVarDec = subRoutineVarDec;
		this.statements = statements;

	}

	@Override
	public void writeXML(JackXMLWriter writer) throws IOException {

		writer.startTag(SUBROUTINE_BODY);

		openingBracket.writeXML(writer);

		for (SubroutineVariableDeceleration subroutineVariableDeceleration : subRoutineVarDec) {
			subroutineVariableDeceleration.writeXML(writer);

		}

		statements.writeXML(writer);

		closingBracket.writeXML(writer);

		writer.endTag(SUBROUTINE_BODY);
	}

	@Override
	public String toString() {

		return "SubroutineBody{" +
				"subRoutineVarDec=" + subRoutineVarDec +
				", statements=" + statements +
				'}';
	}
}
