package jack.analyzer.subroutine.body;

import jack.analyzer.JackParserException;
import jack.analyzer.JackTokenFeeder;
import jack.analyzer.statements.JackStatements;
import jack.analyzer.statements.JackStatementsParser;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.utils.JackUtils;
import jack.analyzer.variable_deceleration.SubroutineVariableDeceleration;
import jack.analyzer.variable_deceleration.SubroutineVariableDecelerationParser;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineBodyParser {

	public static SubroutineBody parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!JackUtils.isOpeningCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new SubroutineBodyParserException();
		}

		ArrayList<SubroutineVariableDeceleration> subRoutineVarDec = new ArrayList<>();

		SubroutineVariableDeceleration varDec;
		while ((varDec = SubroutineVariableDecelerationParser.parse(jackTokenFeeder)) != null) {
			subRoutineVarDec.add(varDec);
		}

		JackStatements statements = JackStatementsParser.parse(jackTokenFeeder);

		if (!JackUtils.isClosingCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new SubroutineBodyParserException();
		}

		return new SubroutineBody(subRoutineVarDec, statements);
	}

}
