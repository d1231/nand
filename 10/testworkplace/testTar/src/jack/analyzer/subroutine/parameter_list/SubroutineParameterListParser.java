package jack.analyzer.subroutine.parameter_list;

import jack.analyzer.JackTokenFeeder;
import jack.analyzer.subroutine.SubroutineParserException;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.utils.JackUtils;
import jack.analyzer.utils.Tuple;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineParameterListParser {

	public static SubroutineParameterList parse(JackTokenFeeder jackTokenFeeder) throws IOException,
			JackTokenizerException, SubroutineParserException {

		SubroutineParameterListBuilder builder = new SubroutineParameterListBuilder();
		JackToken token = jackTokenFeeder.peekNext();

		if (!JackUtils.isType(token)) {

			return builder.build();
		}

		builder.addParameter(jackTokenFeeder.consumeNext(), jackTokenFeeder.consumeNext());

		while (JackUtils.isSeparator(jackTokenFeeder.peekNext())) {

			jackTokenFeeder.consumeNext(); // consume separator
			builder.addParameter(jackTokenFeeder.consumeNext(), jackTokenFeeder.consumeNext());
		}

		return builder.build();

	}

	private static class SubroutineParameterListBuilder {

		private ArrayList<Tuple<JackToken, JackToken>> params = new ArrayList<>();

		public void addParameter(JackToken type, JackToken name) throws SubroutineParserException {

			if (JackUtils.isType(type) && JackUtils.isValidName(name)) {

				params.add(new Tuple<>(type, name));

			} else {

				throw new SubroutineParserException();
			}

		}

		public SubroutineParameterList build() {

			return new SubroutineParameterList(params);
		}
	}
}
