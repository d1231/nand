package jack.analyzer.tokenizer;

import jack.analyzer.JackTokenFeeder;
import jack.analyzer.tokenizer.token.*;

import java.io.*;

/**
 * Created by danny on 10/04/15.
 */
public class JackTokenizer implements AutoCloseable, JackTokenFeeder {


	private final BufferedReader reader;

	private final StreamTokenizer tokenizer;

	private int tokenizerType;

	private JackToken savedToken;

	public JackTokenizer(File file) throws FileNotFoundException {

		reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
		tokenizer = new StreamTokenizer(reader);

		// remove comments
		tokenizer.slashSlashComments(true);
		tokenizer.slashStarComments(true);

		// ignore end of line
		tokenizer.eolIsSignificant(false);

		tokenizer.quoteChar('"');
		tokenizer.wordChars('_', '_'); // add _ to words

		for (char c : JackSymbol.getAllSymbols()) {
			tokenizer.ordinaryChar(c);
		}


	}

	@Override
	public void close() throws IOException {

		reader.close();
	}

	@Override
	public JackToken peekNextNext() throws IOException, JackTokenizerException {

		JackToken lSavedToken = consumeNext();

		JackToken retVal = peekNext();

		savedToken = lSavedToken;

		return retVal;

	}

	@Override
	public JackToken consumeNext() throws IOException, JackTokenizerException {

		JackToken retVal;
		if (savedToken != null) {
			retVal = savedToken;
			savedToken = null;
		} else {
			if (moveToNext()) {
				retVal = get();
			} else {
				retVal = null;
			}
		}
		return retVal;

	}

	public boolean moveToNext() throws IOException {

		return (tokenizerType = tokenizer.nextToken()) != StreamTokenizer.TT_EOF;

	}

	private JackToken get() throws JackTokenizerException {

		final String sval = tokenizer.sval;

		JackToken retVal;
		switch (tokenizerType) {
			case StreamTokenizer.TT_NUMBER:
				retVal = new JackIntegerConstant((int) tokenizer.nval);
				break;
			case StreamTokenizer.TT_WORD:
				retVal = JackKeyword.factory(sval);
				if (retVal == null) {
					retVal = new JackIdentifier(sval);
				}
				break;
			case '"':
				retVal = new JackStringConstant(sval);
				break;
			default:
				retVal = JackSymbol.factory((char) tokenizer.ttype);
				if (retVal == null)
					throw new JackTokenizerException("Unknown symbol");
				break;
		}

		return retVal;

	}

	@Override
	public JackToken peekNext() throws IOException, JackTokenizerException {

		JackToken retVal = consumeNext();
		tokenizer.pushBack();
		return retVal;
	}

}
