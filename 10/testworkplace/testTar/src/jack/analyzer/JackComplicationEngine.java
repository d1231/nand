package jack.analyzer;

import jack.analyzer.jack_class.JackClass;
import jack.analyzer.jack_class.JackClassParser;
import jack.analyzer.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 13/04/15.
 */
public class JackComplicationEngine {

	private final JackTokenFeeder feeder;

	public JackComplicationEngine(JackTokenFeeder feeder) {

		this.feeder = feeder;

	}

	public JackClass compile() throws JackParserException, IOException, JackTokenizerException {

		return JackClassParser.parse(feeder);
	}
}
