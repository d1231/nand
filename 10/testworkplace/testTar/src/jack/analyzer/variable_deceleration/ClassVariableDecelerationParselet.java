package jack.analyzer.variable_deceleration;

import jack.analyzer.JackTokenFeeder;
import jack.analyzer.tokenizer.JackTokenizerException;
import jack.analyzer.tokenizer.token.JackToken;
import jack.analyzer.tokenizer.token.JackTokenType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danny on 18/04/15.
 */
public class ClassVariableDecelerationParselet {

	public static ClassVariableDeceleration parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, VariableDecelerationException {

		ClassVariableDecelerationBuilder builder = new ClassVariableDecelerationBuilder();

		if (!builder.setType(jackTokenFeeder.peekNext())) {
			return null;
		}

		jackTokenFeeder.consumeNext();

		BaseVariableDeceleration variableDeceleration = BaseVariableDecelerationParselet.parse(jackTokenFeeder);

		builder.setVariableDeceleration(variableDeceleration);

		return builder.build();

	}

	private static class ClassVariableDecelerationBuilder {

		private final static Set<String> types = new HashSet<String>() {{
			add("static");
			add("field");
		}};

		private JackToken type;

		private BaseVariableDeceleration variableDeceleration;

		public boolean setType(JackToken jackToken) {

			if (jackToken.getTokenType().equals(JackTokenType.KEYWORD) && types.contains(jackToken.getValue())) {
				type = jackToken;
				return true;
			} else {
				return false;
			}
		}

		public void setVariableDeceleration(BaseVariableDeceleration variableDeceleration) {

			this.variableDeceleration = variableDeceleration;
		}

		public ClassVariableDeceleration build() {

			return new ClassVariableDeceleration(type, variableDeceleration);

		}
	}

}
