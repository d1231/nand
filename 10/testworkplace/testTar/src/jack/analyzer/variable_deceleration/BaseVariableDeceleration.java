package jack.analyzer.variable_deceleration;

import jack.analyzer.JackElement;
import jack.analyzer.JackXMLWriter;
import jack.analyzer.tokenizer.token.JackSymbol;
import jack.analyzer.tokenizer.token.JackToken;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class BaseVariableDeceleration implements JackElement {

	private final static JackToken separator = JackSymbol.factory(',');

	private final static JackToken end = JackSymbol.factory(';');

	private final JackToken type;

	private final ArrayList<JackToken> varNames;

	public BaseVariableDeceleration(JackToken type, ArrayList<JackToken> varNames) {

		this.type = type;
		this.varNames = varNames;
	}


	public void writeXML(JackXMLWriter writer) throws IOException {

		type.writeXML(writer);

		varNames.get(0).writeXML(writer);

		for (int i = 1; i < varNames.size(); i++) {
			separator.writeXML(writer);
			varNames.get(i).writeXML(writer);
		}


		end.writeXML(writer);

	}

	@Override
	public String toString() {

		return "BaseVariableDeceleration{" +
				"type=" + type +
				", varNames=" + varNames +
				'}';
	}
}
