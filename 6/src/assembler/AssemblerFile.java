package assembler;

import assembler.command.AssemblerCommand;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by danny on 14/03/15.
 */
public class AssemblerFile {

    private final List<AssemblerCommand> commands = new LinkedList<>();

    private final SymbolTable table = new SymbolTable();
    private final File file;

    /**
     * @param file
     */
    public AssemblerFile(File file) {

        this.file = file;

    }

    /**
     * Add an assembler command to file
     *
     * @param command
     */
    public void addCommand(AssemblerCommand command) {

        command.init(this);

        commands.add(command);

    }

    /**
     * Creates a hack file from the assembler file in the same directory.
     *
     * @throws IOException
     */
    public void writeHackFile() throws IOException {

        String name = file.getAbsolutePath();
        int i = name.indexOf(".asm");
        String s1 = name.substring(0, i > 0 ? i : name.length()) + ".hack";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(s1))) {

            for (AssemblerCommand command : commands) {

                String s = command.toBinary(table);

                if (s != null && !s.equals("")) {
                    writer.write(s + "\n");
                }

            }

        } catch (IOException x) {
            throw x;
        }
    }

    public SymbolTable getTable() {
        return table;
    }

    public File getFile() {
        return file;
    }
}
