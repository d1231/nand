package assembler;

/**
 * Created by danny on 14/03/15.
 */
public class AssemblerParserException extends Exception {
    public AssemblerParserException(String message) {
        super(message);
    }
}
