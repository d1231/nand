package assembler;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by danny on 27/03/15.
 */
public class Main {

    public static final String ASM = ".asm";

    public static void main(String[] args) {

        if (args.length == 0) {
            System.err.println("Please provide argument");
            return;
        }

        File[] asmFiles = getASMFiles(args[0]);

        createHackFiles(asmFiles);

    }

    private static void createHackFiles(File[] files) {

        for (File f : files) {

            Assembler assembler = new Assembler(f);

            try {

                AssemblerFile file = assembler.assemble();

                file.writeHackFile();

                System.out.println("Successfully create hack file for assembler file: " + f.getName());

            } catch (Exception e) {
                System.err.println("There was an error: ");
                System.err.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private static File[] getASMFiles(String arg) {
        File f = new File(arg);

        File[] files;
        if (f.isDirectory()) {

            files = f.listFiles((dir, name) -> {
                return name.endsWith(ASM);
            });

        } else {
            files = new File[] {f};
        }

        return files;
    }
}
