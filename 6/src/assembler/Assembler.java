package assembler;

import assembler.command.AssemblerCommand;

import java.io.File;

/**
 * Created by danny on 12/03/15.
 */
public class Assembler {

    private final File file;

    public Assembler(File file) {

        this.file = file;
    }

    /**
     * Create an assembler file from the object
     */
    public AssemblerFile assemble() throws Exception {

        AssemblerFile assemblerFile = new AssemblerFile(file);

        try (AssemblerParser assemblerParser = new AssemblerParser(assemblerFile)) {

            while (assemblerParser.moveToNext()) {

                AssemblerCommand command = assemblerParser.get();

                assemblerFile.addCommand(command);

            }

            return assemblerFile;

        } catch (Exception e) {
            throw e;
        }

    }
}
