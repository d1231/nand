package assembler;

import assembler.command.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by danny on 12/03/15.
 */
public class AssemblerParser implements AutoCloseable {

    private static final String COMMENT_REGEX = "//.*";

    private static final String L_INSTRUCTION_REGEX = "\\(([\\w\\.\\$:][\\w\\d\\.\\$:]*)\\)";
    private static final String A_INSTRUCTION_REGEX = "@(\\d+|[\\w\\.\\$:][\\w\\.\\$:\\d]*)";

    private static final String DESTNATION_REGEX = "(((A?)(M?)(D?))=)";

    private static final Pattern lInstructionPattern;

    private static final Pattern aInstructionPattern;

    private static final Pattern destPattern;

    private static final Pattern commentPattern;

    static {
        lInstructionPattern = Pattern.compile(L_INSTRUCTION_REGEX);
        aInstructionPattern = Pattern.compile(A_INSTRUCTION_REGEX);
        destPattern = Pattern.compile(DESTNATION_REGEX);
        commentPattern = Pattern.compile(COMMENT_REGEX);
    }

    private final BufferedReader reader;
    private String line;
    private int lineCounter = -1;

    /**
     * Creates an assembler parser to assembler file
     *
     * @param file
     * @throws IOException
     */
    public AssemblerParser(AssemblerFile file) throws IOException {

        reader = new BufferedReader(new FileReader(file.getFile().getAbsoluteFile()));

    }

    /**
     * @return true iff there are another line inside file
     * @throws IOException
     */
    public boolean moveToNext() throws IOException {

        while (true) {
            String s = reader.readLine();

            // EOF
            if (s == null)
                return false;

            s = s.replaceAll("\\s+", "");

            if (s.equals("")) {
                continue;
            }

            Matcher m = commentPattern.matcher(s);
            if (m.matches()) {
                continue;

            }

            if (m.find()) {
                s = s.substring(0, m.start());
            }


            line = s;

            return true;

        }
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }

    /**
     * Returns the current AssemblerCommand the parser points to.
     * <p>
     * In case of an illegal command will throw an exception.
     *
     * @return
     * @throws AssemblerParserException
     */
    public AssemblerCommand get() throws AssemblerParserException {


        Matcher m = lInstructionPattern.matcher(line);
        if (m.matches()) { // if is L command

            return new LCommand(m.group(1), lineCounter + 1);

        }

        m = aInstructionPattern.matcher(line);
        if (m.matches()) { // if is A command

            lineCounter++;
            return new ACommand(m.group(1));
        }

        CCommand cCommand = parseCInstruction(line);
        if (cCommand != null) {

            lineCounter++;

        }

        return cCommand;

    }

    private static CCommand parseCInstruction(String s) throws AssemblerParserException {

        CCommandBuilder builder = new CCommandBuilder();

        s = parseDest(s, builder);

        s = parseComp(s, builder);

        parseJump(s, builder);

        return builder.createCCommand();
    }

    private static void parseJump(String s, CCommandBuilder builder) {

        builder.setJump(s);

    }

    private static String parseComp(String s, CCommandBuilder builder) throws AssemblerParserException {

        int i = s.indexOf(';');

        if (i == 0) {
            throw new AssemblerParserException("Expected expression");
        } else if (i > 0) {
            builder.setComp(s.substring(0, i));
            return s.substring(i + 1);
        } else {
            builder.setComp(s);
            return "";
        }


    }

    private static String parseDest(String s, CCommandBuilder builder) throws AssemblerParserException {

        int i = s.indexOf('=');

        if (i == -1) {
            return s;
        }

        Matcher destMatcher = destPattern.matcher(s);

        if (destMatcher.find()) {

            if (destMatcher.group(3).length() > 0) {
                builder.addDestination(CCommandBuilder.Destination.A);
            }

            if (destMatcher.group(4).length() > 0) {
                builder.addDestination(CCommandBuilder.Destination.M);
            }


            if (destMatcher.group(5).length() > 0) {
                builder.addDestination(CCommandBuilder.Destination.D);
            }

            return s.substring(i + 1);

        } else {
            throw new AssemblerParserException("Expected destination");
        }


    }


}
