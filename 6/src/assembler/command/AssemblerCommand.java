package assembler.command;

import assembler.AssemblerFile;
import assembler.SymbolTable;

/**
 * Represent an interface for an Hack Assembly command
 * <p>
 * Created by danny on 12/03/15.
 */
public interface AssemblerCommand {


    /**
     * Returns the binary representation of the command. Return null or empty string to indicate that there is no
     * binary representation.
     *
     * @param table
     * @return
     */
    String toBinary(SymbolTable table);

    /**
     * Initializing the command
     *
     * @param table
     */
    void init(AssemblerFile table);
}
