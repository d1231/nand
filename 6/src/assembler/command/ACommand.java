package assembler.command;

import assembler.AssemblerFile;
import assembler.SymbolTable;

/**
 * Created by danny on 13/03/15.
 */
public class ACommand implements AssemblerCommand {

    public static final char A_INSTRUCTION_BIT = '0';
    private final String address;

    /**
     * Creates an ACommand
     *
     * @param address address to load
     */
    public ACommand(String address) {
        this.address = address;
    }

    @Override
    public String toBinary(SymbolTable table) {

        int val;

        if (isNumeric(address)) {
            val = Integer.parseInt(address);
        } else {
            val = table.getAddress(address);
        }

        StringBuilder sb = new StringBuilder(16);

        sb.append(A_INSTRUCTION_BIT);
        sb.append(String.format("%15s", Integer.toBinaryString(val)).replaceAll(" ", "0"));

        return sb.toString();
    }

    /**
     * @param str
     * @return true iff str is an numeric string
     */
    public static boolean isNumeric(String str) {

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;

    }

    @Override
    public void init(AssemblerFile table) {

    }

    @Override
    public String toString() {
        return "ACommand{" +
                "address='" + address + '\'' +
                '}';
    }

}
