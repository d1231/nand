package assembler.command;

import assembler.AssemblerFile;
import assembler.AssemblerParserException;
import assembler.SymbolTable;

import java.util.HashMap;

/**
 * Created by danny on 12/03/15.
 */
public class CCommand implements AssemblerCommand {

    public static final String SHIFT_CINSTRUCTION_CODE = "101";
    public static final String SHIFT_CINSTRUCTION_CODE2 = "0000";

    private static final String CINSTRUCTION_CODE = "111";

    private final static HashMap<String, String> compTranslation = new HashMap<String, String>() {{
        put("0", CINSTRUCTION_CODE + "0101010");
        put("1", CINSTRUCTION_CODE + "0111111");
        put("-1", CINSTRUCTION_CODE + "0111010");
        put("D", CINSTRUCTION_CODE + "0001100");
        put("A", CINSTRUCTION_CODE + "0110000");
        put("M", CINSTRUCTION_CODE + "1110000");
        put("!D", CINSTRUCTION_CODE + "0001101");
        put("!A", CINSTRUCTION_CODE + "0110001");
        put("!M", CINSTRUCTION_CODE + "1110001");
        put("-D", CINSTRUCTION_CODE + "0001111");
        put("-A", CINSTRUCTION_CODE + "0110011");
        put("-M", CINSTRUCTION_CODE + "1110011");
        put("D+1", CINSTRUCTION_CODE + "0011111");
        put("A+1", CINSTRUCTION_CODE + "0110111");
        put("M+1", CINSTRUCTION_CODE + "1110111");
        put("D-1", CINSTRUCTION_CODE + "0001110");
        put("A-1", CINSTRUCTION_CODE + "0110010");
        put("M-1", CINSTRUCTION_CODE + "1110010");
        put("D+A", CINSTRUCTION_CODE + "0000010");
        put("A+D", CINSTRUCTION_CODE + "0000010");
        put("D+M", CINSTRUCTION_CODE + "1000010");
        put("M+D", CINSTRUCTION_CODE + "1000010");
        put("D-A", CINSTRUCTION_CODE + "0010011");
        put("D-M", CINSTRUCTION_CODE + "1010011");
        put("A-D", CINSTRUCTION_CODE + "0000111");
        put("M-D", CINSTRUCTION_CODE + "1000111");
        put("D&A", CINSTRUCTION_CODE + "0000000");
        put("A&D", CINSTRUCTION_CODE + "0000000");
        put("D&M", CINSTRUCTION_CODE + "1000000");
        put("M&D", CINSTRUCTION_CODE + "1000000");
        put("D|A", CINSTRUCTION_CODE + "0010101");
        put("A|D", CINSTRUCTION_CODE + "0010101");
        put("D|M", CINSTRUCTION_CODE + "1010101");
        put("M|D", CINSTRUCTION_CODE + "1010101");
        put("D<<", SHIFT_CINSTRUCTION_CODE + "011" + SHIFT_CINSTRUCTION_CODE2);
        put("A<<", SHIFT_CINSTRUCTION_CODE + "010" + SHIFT_CINSTRUCTION_CODE2);
        put("M<<", SHIFT_CINSTRUCTION_CODE + "110" + SHIFT_CINSTRUCTION_CODE2);
        put("D>>", SHIFT_CINSTRUCTION_CODE + "001" + SHIFT_CINSTRUCTION_CODE2);
        put("A>>", SHIFT_CINSTRUCTION_CODE + "000" + SHIFT_CINSTRUCTION_CODE2);
        put("M>>", SHIFT_CINSTRUCTION_CODE + "100" + SHIFT_CINSTRUCTION_CODE2);

    }};
    private final static HashMap<String, String> jmpTranslation = new HashMap<String, String>() {{
        put("", "000");
        put("null", "000");
        put("JGT", "001");
        put("JEQ", "010");
        put("JGE", "011");
        put("JLT", "100");
        put("JNE", "101");
        put("JLE", "110");
        put("JMP", "111");

    }};

    private final boolean destD;

    private final boolean destA;

    private final boolean destM;

    private final String jump;

    private final String comp;


    /**
     * Creates an CCommand
     * @param destA if A is in destination
     * @param destD if D is in destination
     * @param destM if D is in destination
     * @param comp the comp part of the command
     * @param jump the jump part of the command
     * @throws AssemblerParserException
     */
    public CCommand(boolean destA, boolean destD, boolean destM, String comp, String jump) throws
            AssemblerParserException {

        this.destA = destA;
        this.destD = destD;
        this.destM = destM;

        this.comp = compTranslation.get(comp);
        this.jump = jmpTranslation.get(jump);

        if (this.comp == null || (jump != null && this.jump == null)) {
            throw new AssemblerParserException("Expected expression");
        }

    }

    @Override
    public String toBinary(SymbolTable table) {

        StringBuilder sb = new StringBuilder();

        sb.append(comp);
        sb.append(destA ? "1" : "0");
        sb.append(destM ? "1" : "0");
        sb.append(destD ? "1" : "0");
        sb.append(jump == null ? "000" : jump);

        return sb.toString();
    }

    @Override
    public void init(AssemblerFile table) {

    }

    @Override
    public String toString() {
        return "CCommand{" +
                "destD=" + destD +
                ", destA=" + destA +
                ", destM=" + destM +
                ", jump='" + jump + '\'' +
                ", comp='" + comp + '\'' +
                '}';
    }


}
