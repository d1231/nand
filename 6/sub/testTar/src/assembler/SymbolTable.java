package assembler;

import java.util.HashMap;

/**
 * Created by danny on 13/03/15.
 */
public class SymbolTable {

    private final static HashMap<String, Integer> predefinedSymbol = new HashMap<String, Integer>() {{

        put("SP", 0x0);
        put("LCL", 0x1);
        put("ARG", 0x2);
        put("THIS", 0x3);
        put("THAT", 0x4);
        put("R0", 0x0);
        put("R1", 0x1);
        put("R2", 0x2);
        put("R3", 0x3);
        put("R4", 0x4);
        put("R5", 0x5);
        put("R6", 0x6);
        put("R7", 0x7);
        put("R8", 0x8);
        put("R9", 0x9);
        put("R10", 0xA);
        put("R11", 0xB);
        put("R12", 0xC);
        put("R13", 0xD);
        put("R14", 0xE);
        put("R15", 0xF);
        put("SCREEN", 0x4000);
        put("KBD", 0x6000);

    }};

    private final HashMap<String, Integer> table;

    private int counter = 0x10;

    public SymbolTable() {

        table = new HashMap<>(predefinedSymbol);

    }

    public void addSymbol(String symbol, int address) {

        if (!table.containsKey(symbol)) {
            table.put(symbol, address);
        }

    }

    /**
     * Returns the matching ROM address for symbol
     * @param symbol
     * @return
     */
    public int getAddress(String symbol) {

        Integer val = table.get(symbol);

        // if not exists create an address for the symbol
        if (val == null) {
            val = counter++;
            addSymbol(symbol, val);
        }

        return val;
    }
}
