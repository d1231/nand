package assembler.command;

import assembler.AssemblerParserException;

/**
 * Helper class to build ccmonmands
 */
public class CCommandBuilder {

    private boolean destA;
    private boolean destD;
    private boolean destM;
    private String comp;
    private String jump;

    public CCommandBuilder() {

    }

    public CCommandBuilder setComp(String comp) {
        this.comp = comp;
        return this;
    }

    public CCommandBuilder setJump(String jump) {

        this.jump = jump;
        return this;
    }

    public CCommand createCCommand() throws AssemblerParserException {

        return new CCommand(destA, destD, destM, comp, jump);
    }

    public CCommandBuilder addDestination(Destination m) {

        switch (m) {
            case A:
                destA = true;
                break;
            case M:
                destD = true;
                break;
            case D:
                destM = true;
                break;
        }

        return this;
    }

    public enum Destination {
        A, M, D
    }
}