package assembler.command;

import assembler.AssemblerFile;
import assembler.SymbolTable;

/**
 * Created by danny on 13/03/15.
 */
public class LCommand implements AssemblerCommand {

    private final String label;
    private final int line;

    /**
     * Creates Load command.
     *
     * @param label the label declared
     * @param line  the line it was declared
     */
    public LCommand(String label, int line) {
        this.label = label;
        this.line = line;
    }

    @Override
    public String toBinary(SymbolTable table) {

        return null;
    }

    @Override
    public void init(AssemblerFile file) {

        file.getTable().addSymbol(label, line);
    }
}
