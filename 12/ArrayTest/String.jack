// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/12/String.jack

/**
 * Represents a String object. Implements the String type.
 */
class String {

    field Array val;
    field int len, maxLen;

    /** Constructs a new empty String with a maximum length of maxLength. */
    constructor String new(int maxLength) {

        let val = Array.new(maxLength);
        let len = 0;
        let maxLen = maxLength;

        return this;
    }

    /** De-allocates the string and frees its space. */
    method void dispose() {

        do val.dispose();

        return;
    }

    /** Returns the current length of this String. */
    method int length() {

        return len;

    }

    /** Returns the character at location j. */
    method char charAt(int j) {

        return val[j];

    }

    /** Sets the j'th character of this string to be c. */
    method void setCharAt(int j, char c) {

        let val[j] = c;

        return;
    }

    /** Appends the character c to the end of this String.
     *  Returns this string as the return value. */
    method String appendChar(char c) {

        let val[len] = c;
        let len = len + 1;

        return this;
    }

    /** Erases the last character from this String. */
    method void eraseLastChar() {

        let len = len - 1;

        return;

    }

    /** Returns the integer value of this String until the first non
     *  numeric character. */
    method int intValue() {

        var int intVal;
        var int i, d;
        var bool pos;

        if (val[i] = 45) {
            let pos = false;
            let i = i + 1;
        } else {
            let pos = true;
        }


        while (i < len) {
            if ((val[i] < 48) | (val[i] > 57)) {

                let i = len + 1;

            } else {

                let d = val[i] - 48;
                let intVal = intVal * 10 + d;

            }
        }

        if (pos) {
            return intVal;
        } else {
            return -intVal;
        }

    }

    /** Sets this String to hold a representation of the given number. */
    method void setInt(int number) {

        var char lastDigit;
        var char lastChar;

        let len = 0;

        if (number < 0) {
            do appendChar(45);
            let number = -number;
        }

        while (~(number = 0)) {
            
            let lastDigit = Math.mod(number, 10);
            let lastChar = lastDigit + 48;
            do appendChar(lastChar);
            let number = number / 10;

        }

        return;

    }

    /** Returns the new line character. */
    function char newLine() {
        
        return 128;

    }

    /** Returns the backspace character. */
    function char backSpace() {

        return 129;

    }

    /** Returns the double quote (") character. */
    function char doubleQuote() {

        return 34;

    }
}
