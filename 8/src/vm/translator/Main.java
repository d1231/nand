package vm.translator;

import java.util.Objects;

/**
 * Created by danny on 22/03/15.
 */
public class Main {


	public static void main(String[] args) {

		if (args.length == 0) {
			System.err.println("Please provide argument");
			return;
		}


		String arg = args[0];

		boolean bootStrap = args.length > 1 && Objects.equals(args[1], "-d");
		System.out.println("Without bootstrap: " + bootStrap);

		VMTranslator VMTranslator = new VMTranslator(arg);

		try {
			String file = VMTranslator.createASMFile();

			System.out.println("OK create asm file: " + file);
		} catch (Exception e) {
			System.err.println("ERROR: " + e.toString());
		}


	}


}
