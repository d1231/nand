package vm.translator.command.memory_access;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 21/03/15.
 */
public class PushVMCommand implements VMCommand {


	private static final String NEW_LINE = "\n";

	private final static String PUSH_POINTER = "@%s\n" + "D=M";

	private final static String PUSH_ADDRESS = "@%s\n" + "D=A";

	private final static String STATIC = "@%s.%s" + NEW_LINE + "D=M";

	private final static String CONSTANT = "@%s" + NEW_LINE + "D=A";

	private final static String POINTER_ACCESS = "@%s" + NEW_LINE + "D=M" + NEW_LINE + "@%s" + NEW_LINE + "A=A+D" +
			NEW_LINE + "D=M";

	private final static String ARRAY_ACCESS = "@%s" + NEW_LINE + "D=A" + NEW_LINE + "@%s" + NEW_LINE + "A=A+D" +
			NEW_LINE + "D=M";

	private final static String PUSH_COMMAND = "@SP" + NEW_LINE + "A=M" + NEW_LINE + "M=D" + NEW_LINE + "@SP" +
			NEW_LINE + "M=M+1";


	private final String value;

	private final Segment segment;

	public PushVMCommand(Segment segment, String value) {

		this.segment = segment;
		this.value = value;
	}

	@Override
	public String toAssembly(VMContext context) {

		StringBuilder sb = new StringBuilder();

		switch (segment) {
			case ARGUMENT:
			case LOCAL:
			case THIS:
			case THAT:
				sb.append(String.format(POINTER_ACCESS, segment.getSegment(), value));
				break;
			case STATIC:
				sb.append(String.format(STATIC, context.getFileName(), value));
				break;
			case CONSTANT:
				sb.append(String.format(CONSTANT, value));
				break;
			case POINTER:
			case TEMP:
				sb.append(String.format(ARRAY_ACCESS, value, segment.getSegment()));
				break;
			case ADDRESS:
				sb.append(String.format(PUSH_ADDRESS, value));
				break;
			case ADDRESS_POINTER:
				sb.append(String.format(PUSH_POINTER, value));
				break;
		}

		sb.append(NEW_LINE);
		sb.append(PUSH_COMMAND);

		return sb.toString();
	}
}
