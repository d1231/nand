package vm.translator.command.memory_access;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 21/03/15.
 */
public class PopVMCommand implements VMCommand {

	private static final String NEW_LINE = "\n";

	private final static String POINTER_ACCESS = "@%s" + NEW_LINE + "D=M" + NEW_LINE + "@%s" + NEW_LINE + "D=A+D" +
			NEW_LINE + "@R13" + NEW_LINE + "M=D";

	private final static String POP_COMMAND = "@SP" + NEW_LINE + "AM=M-1" + NEW_LINE + "D=M";

	private final static String POP_TO_COMMAND = "@R13" + NEW_LINE + "A=M" + NEW_LINE + "M=D";

	private final static String STATIC_POP_TO = "@%s.%s" + NEW_LINE + "M=D";

	private final static String ARRAY_ACCESS = "@%s" + NEW_LINE + "D=A" + NEW_LINE + "@%s" + NEW_LINE + "D=A+D" +
			NEW_LINE + "@R13" + NEW_LINE + "M=D";

	private static final String ADDRESS_POP_TO_COMMAND = "@%s" + NEW_LINE + "M=D";


	private final String value;

	private final Segment segment;

	public PopVMCommand(Segment segment, String value) {

		this.segment = segment;
		this.value = value;
	}

	@Override
	public String toAssembly(VMContext context) {

		StringBuilder sb = new StringBuilder();

		switch (segment) {
			case ARGUMENT:
			case LOCAL:
			case THIS:
			case THAT:
				sb.append(String.format(POINTER_ACCESS, segment.getSegment(), value));
				sb.append(NEW_LINE);
				sb.append(POP_COMMAND);
				sb.append(NEW_LINE);
				sb.append(POP_TO_COMMAND);
				break;
			case STATIC:
				sb.append(POP_COMMAND);
				sb.append(NEW_LINE);
				sb.append(String.format(STATIC_POP_TO, context.getFileName(), value));
				break;
			case POINTER:
			case TEMP:
				sb.append(String.format(ARRAY_ACCESS, segment.getSegment(), value));
				sb.append(NEW_LINE);
				sb.append(POP_COMMAND);
				sb.append(NEW_LINE);
				sb.append(POP_TO_COMMAND);
				break;
			case ADDRESS:
				sb.append(POP_COMMAND);
				sb.append(NEW_LINE);
				sb.append(String.format(ADDRESS_POP_TO_COMMAND, value));
				break;
		}

		return sb.toString();
	}

}
