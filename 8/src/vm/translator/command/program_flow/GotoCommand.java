package vm.translator.command.program_flow;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 03/04/15.
 */
public class GotoCommand implements VMCommand {

    private static final String NEW_LINE = "\n";

    private final static String COMMAND = "@%s" + NEW_LINE + "0;JMP";
    private final String label;

    public GotoCommand(String label) {

        this.label = label;
    }


    @Override
    public String toAssembly(VMContext context) {

        String label = context.getLabel(this.label);

        return String.format(COMMAND, label);

    }
}
