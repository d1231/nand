package vm.translator.command.program_flow;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 03/04/15.
 */
public class IfGotoCommand implements VMCommand {

	private final static String COMMAND = "@SP\n" +
			"AM=M-1\n" +
			"D=M\n" +
			"@%s\n" +
			"D;JNE";

	private final String label;

	public IfGotoCommand(String label) {

		this.label = label;
	}


	@Override
	public String toAssembly(VMContext context) {

		String label = context.getLabel(this.label);

		return String.format(COMMAND, label);
	}
}
