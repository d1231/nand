package vm.translator.command.program_flow;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 03/04/15.
 */
public class LabelCommand implements VMCommand {


    private final static String LABEL_COMMAND = "(%s)";
    private final String label;

    public LabelCommand(String label) {
        this.label = label;
    }

    @Override
    public String toAssembly(VMContext context) {

        String s = context.getLabel(label);
        return String.format(LABEL_COMMAND, s);
    }
}
