package vm.translator.command.arithmetic;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 21/03/15.
 */
public class JumpArithmeticCommand implements VMCommand {

	private static final String SAVE_TOP = "@SP\n" +
			"AM=M-1\n" +
			"D=M\n" +
			"@R13\n" +
			"M=D\n" +
			"@SP\n" +
			"A=M-1\n" +
			"D=M\n" +
			"@R14\n" +
			"M=D\n";

	private static final String COMMAND2 = "@SP\n" +
			"A=M-1\n" +
			"D=M\n" +
			"@R14\n" +
			"M=1\n" +
			"@%s\n" +
			"D;JGT\n" +
			"@R14\n" +
			"M=0\n" +
			"@%s\n" +
			"D;JEQ\n" +
			"@R14\n" +
			"M=-1\n" +
			"(%s)\n";

	private static final String COMMAND3 = "@R13\n" +
			"D=M\n" +
			"@R15\n" +
			"M=1\n" +
			"@%s\n" +
			"D;JGT\n" +
			"@R15\n" +
			"M=0\n" +
			"@%s\n" +
			"D;JEQ\n" +
			"@R15\n" +
			"M=-1\n" +
			"(%s)\n";

	private static final String COMMAND4 = "@R15\n" +
			"D=M\n" +
			"@R14\n" +
			"D=M-D\n" +
			"@%s\n" +
			"D;JEQ\n";


	private static final String GREATER_THAN_COMMAND = "@SP\n" +
			"A=M-1\n" +
			"M=-1\n" +
			"@%s\n" +
			"D;JGT\n" +
			"@SP\n" +
			"A=M-1\n" +
			"M=0\n" +
			"@%s\n" +
			"0;JMP\n" +
			"(%s)\n";

	private static final String LESSER_THAN_COMMAND = "@SP\n" +
			"A=M-1\n" +
			"M=-1\n" +
			"@%s\n" +
			"D;JLT\n" +
			"@SP\n" +
			"A=M-1\n" +
			"M=0\n" +
			"@%s\n" +
			"0;JMP\n" +
			"(%s)\n";

	private static final String EQUAL_COMMAND = "@SP\n" +
			"A=M-1\n" +
			"M=0\n" +
			"@%s\n" +
			"D;JNE\n" +
			"(%s)\n";

	private static final String COMMAND = "@R13\n" +
			"D=M\n" +
			"@SP\n" +
			"A=M-1\n" +
			"D=M-D\n" +
			"@SP\n" +
			"A=M-1\n" +
			"M=-1\n" +
			"%s\n" +
			"D;%s\n" +
			"@SP\n" +
			"A=M-1\n" +
			"M=0\n" +
			"%s";

	private static final String LABEL_ACOMMAND = "@%s";

	private static final String LABEL_DECELERATION = "(%s)";

	private final JumpOperator operator;

	public JumpArithmeticCommand(JumpOperator operator) {

		this.operator = operator;
	}

	@Override
	public String toAssembly(VMContext context) {

		StringBuilder sb = new StringBuilder();

		sb.append(SAVE_TOP);

		String endLabel = context.getLabel();

		String label2 = context.getLabel();
		String label3 = context.getLabel();
		String label4 = context.getLabel();

		sb.append(String.format(COMMAND2, label3, label3, label3));
		sb.append(String.format(COMMAND3, label4, label4, label4));
		sb.append(String.format(COMMAND4, label2));

		switch (operator) {
			case EQUAL:
				sb.append(String.format(EQUAL_COMMAND, endLabel, label2));
				break;
			case GREATER_THAN:
				sb.append(String.format(GREATER_THAN_COMMAND, endLabel, endLabel, label2));
				break;
			case SMALLER_THAN:
				sb.append(String.format(LESSER_THAN_COMMAND, endLabel, endLabel, label2));
				break;
		}

		sb.append(String.format(COMMAND, String.format(LABEL_ACOMMAND, endLabel), operator.toAssembly(), String.format
				(LABEL_DECELERATION, endLabel)));

		return sb.toString();

	}

	/**
	 * Created by danny on 21/03/15.
	 */
	public static enum JumpOperator {

		EQUAL,
		GREATER_THAN,
		SMALLER_THAN;

		public String toAssembly() {

			switch (this) {
				case EQUAL:
					return "JEQ";
				case GREATER_THAN:
					return "JGT";
				case SMALLER_THAN:
					return "JLT";
			}
			return "";
		}

	}
}
