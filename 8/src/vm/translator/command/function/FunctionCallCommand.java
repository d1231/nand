package vm.translator.command.function;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;
import vm.translator.command.memory_access.PushVMCommand;
import vm.translator.command.memory_access.Segment;

/**
 * Created by danny on 03/04/15.
 */
public class FunctionCallCommand implements VMCommand {

	private final static VMCommand[] SAVE_ENV_VM_COMMANDS = {new PushVMCommand(Segment.ADDRESS_POINTER, "LCL"), new
			PushVMCommand(Segment.ADDRESS_POINTER, "ARG"), new PushVMCommand(Segment.ADDRESS_POINTER, "THIS"), new
			PushVMCommand(Segment.ADDRESS_POINTER, "THAT")};

	private final static String JMP_SEQUENCE = "@SP\n" +
			"D=M\n" +
			"@LCL\n" +
			"M=D\n" +
			"@%d\n" +
			"D=D-A\n" +
			"@ARG\n" +
			"M=D\n" +
			"@%s\n" +
			"0;JMP\n" +
			"(%s)";

	private final String functionName;

	private final int arguments;

	public FunctionCallCommand(String functionName, int argument) {

		this.functionName = functionName;
		this.arguments = argument;
	}

	@Override
	public String toAssembly(VMContext context) {

		String retAddress = context.getLabel(); // get lbl to return address

		PushVMCommand pushRetAddress = new PushVMCommand(Segment.ADDRESS, retAddress);

		StringBuilder sb = new StringBuilder();

		sb.append(pushRetAddress.toAssembly(context)); // save return address

		// save environment
		for (VMCommand vmCommand : SAVE_ENV_VM_COMMANDS) {
			sb.append('\n');
			sb.append(vmCommand.toAssembly(context));
		}
		sb.append('\n');

		// set new lcl, arg and jmp
		sb.append(String.format(JMP_SEQUENCE, arguments + 5, functionName, retAddress));

		return sb.toString();
	}
}
