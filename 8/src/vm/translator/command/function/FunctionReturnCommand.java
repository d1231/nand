package vm.translator.command.function;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;
import vm.translator.command.memory_access.PopVMCommand;
import vm.translator.command.memory_access.Segment;

/**
 * Created by danny on 04/04/15.
 */
public class FunctionReturnCommand implements VMCommand {

	public static final PopVMCommand RETURN_VALUE = new PopVMCommand(Segment.ARGUMENT, "0");

	public FunctionReturnCommand() {

	}

	@Override
	public String toAssembly(VMContext context) {

		StringBuilder sb = new StringBuilder();

		sb.append("\n// return\n");
		sb.append("@LCL\nD=M\n@FRAME\nM=D\n"); // FRAME = LCL
		sb.append("@5\nA=D-A\nD=M\n@RET\nM=D\n"); // RET = *(FRAME-5)
		sb.append(RETURN_VALUE.toAssembly(context)); // *ARG = pop()
		sb.append('\n');
		sb.append("@ARG\nD=M+1\n@SP\nM=D\n"); // SP = ARG + 1
		sb.append("@FRAME\nAM=M-1\nD=M\n@THAT\nM=D\n"); // THAT=*(FRAME-1)
		sb.append("@FRAME\nAM=M-1\nD=M\n@THIS\nM=D\n"); // THIS=*(FRAME-1)
		sb.append("@FRAME\nAM=M-1\nD=M\n@ARG\nM=D\n"); // ARG=*(FRAME-1)
		sb.append("@FRAME\nAM=M-1\nD=M\n@LCL\nM=D\n"); // LCL=*(FRAME-1)
		sb.append("@RET\nA=M\n0;JMP"); // jmp

		return sb.toString();

	}
}
