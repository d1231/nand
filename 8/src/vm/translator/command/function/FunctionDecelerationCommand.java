package vm.translator.command.function;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;
import vm.translator.command.memory_access.PushVMCommand;
import vm.translator.command.memory_access.Segment;

/**
 * Created by danny on 03/04/15.
 */
public class FunctionDecelerationCommand implements VMCommand {

	private static final String COMMAND = "(%s)";

	private static PushVMCommand pushVMCommand = new PushVMCommand(Segment.CONSTANT, "0");

	private final int localVariables;

	private final String functionName;

	public FunctionDecelerationCommand(String functionName, int localVariables) {

		this.functionName = functionName;
		this.localVariables = localVariables;
	}

	@Override
	public String toAssembly(VMContext context) {

		context.setFunction(functionName);

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format(COMMAND, functionName));

		for (int i = 0; i < localVariables; i++) {
			stringBuilder.append("\n" + pushVMCommand.toAssembly(context));
		}

		return stringBuilder.toString();
	}
}
