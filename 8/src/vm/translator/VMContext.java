package vm.translator;

/**
 * Created by danny on 21/03/15.
 */
public class VMContext {


	public static final String LABEL = "%s:%s";

	private final LabelGenerator labelGenerator;

	private final String fileName;

	private String function = null;

	public VMContext(String fileName, LabelGenerator labelGenerator) {

		this.fileName = fileName;

		this.labelGenerator = labelGenerator;

	}

	public String getFileName() {

		return fileName;
	}

	public void setFunction(String function) {

		this.function = function;

	}

	public String getLabel() {

		return labelGenerator.getLabel();
	}

	/**
	 * Get the matching label to label depending on the context
	 *
	 * @param label
	 * @return
	 */
	public String getLabel(String label) {

		String context = function == null ? fileName : function;

		return String.format(LABEL, context, label);
	}


}
