package vm.translator;

import vm.translator.command.VMCommand;
import vm.translator.command.function.FunctionCallCommand;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 20/03/15.
 */
public class VMTranslator {

	public static final String INIT_FUNCTION = "Sys.init";

	public static final String BOOT_STRAP = "BOOT_STRAP";

	private static final String VM_SUFFIX = ".vm";

	private static final String INIT_COMMAND = "@256\nD=A\n@SP\nM=D";

	private static final String ASM_SUFFIX = ".asm";


	private File[] files;

	private String path;


	public VMTranslator(String path) {

		init(path);

	}

	private void init(String arg) {

		File file = new File(arg);

		if (file.isDirectory()) {
			files = file.listFiles((dir, name) -> {
				return name.endsWith(VM_SUFFIX);
			});

			path = file.getAbsolutePath() + "/" + file.getName() + ASM_SUFFIX;

		} else {
			files = new File[] {file};

			String name = file.getAbsolutePath();

			path = name.replace(VM_SUFFIX, "") + ASM_SUFFIX;
		}
	}

	/**
	 * Translate to assembly language the vm files given to translator
	 *
	 * @throws VMFileException
	 * @throws IOException
	 */
	public String createASMFile() throws VMFileException, IOException {

		File output = new File(path);
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {

			LabelGenerator labelGenerator = new LabelGenerator();

//			if (bootStrap)
//				writeBootStrap(writer, labelGenerator);

			writeBootStrap(writer, labelGenerator);

			for (File f : files) {

				String name = f.getName();
				name = name.replace(VM_SUFFIX, "");

				VMContext context = new VMContext(name, labelGenerator);

				ArrayList<VMCommand> list = translate(f);
				for (VMCommand command : list) {
					writer.write(command.toAssembly(context));
					writer.newLine();
				}

			}
		}

		return output.getName();

	}

	private void writeBootStrap(BufferedWriter writer, LabelGenerator labelGenerator) throws IOException {

		writer.write(INIT_COMMAND);
		writer.newLine();
		writer.write(new FunctionCallCommand(INIT_FUNCTION, 0).toAssembly(new VMContext(BOOT_STRAP, labelGenerator)));
		writer.newLine();

	}

	private ArrayList<VMCommand> translate(File f) throws VMFileException {

		ArrayList<VMCommand> vmCommandArrayList = new ArrayList<>();
		try (VMParser parser = new VMParser(f)) {

			while (parser.moveToNext()) {
				vmCommandArrayList.add(parser.get());
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new VMFileException(e.getMessage());
		}

		return vmCommandArrayList;
	}

}
