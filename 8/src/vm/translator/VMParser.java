package vm.translator;

import vm.translator.command.VMCommand;
import vm.translator.command.arithmetic.BinaryArithmeticCommand;
import vm.translator.command.arithmetic.JumpArithmeticCommand;
import vm.translator.command.arithmetic.UnaryVMCommand;
import vm.translator.command.function.FunctionCallCommand;
import vm.translator.command.function.FunctionDecelerationCommand;
import vm.translator.command.function.FunctionReturnCommand;
import vm.translator.command.memory_access.PopVMCommand;
import vm.translator.command.memory_access.PushVMCommand;
import vm.translator.command.memory_access.Segment;
import vm.translator.command.program_flow.GotoCommand;
import vm.translator.command.program_flow.IfGotoCommand;
import vm.translator.command.program_flow.LabelCommand;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by danny on 20/03/15.
 */
public class VMParser implements AutoCloseable {

	private static final String POP = "pop";

	private static final String PUSH = "push";

	private static final String IF_GOTO = "if-goto";

	private static final String GOTO = "goto";

	private static final String LABEL = "label";

	private static final String FUNCTION = "function";

	private static final String CALL = "call";

	private static final String COMMENT_REGEX = "//.*";

	private static final Pattern commentPattern = Pattern.compile(COMMENT_REGEX);

	private final static Map<String, VMCommand> table = new HashMap<String, VMCommand>() {{

		put("add", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.ADD));
		put("sub", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.SUB));
		put("neg", new UnaryVMCommand(UnaryVMCommand.UnaryOperator.NEG));
		put("eq", new JumpArithmeticCommand(JumpArithmeticCommand.JumpOperator.EQUAL));
		put("gt", new JumpArithmeticCommand(JumpArithmeticCommand.JumpOperator.GREATER_THAN));
		put("lt", new JumpArithmeticCommand(JumpArithmeticCommand.JumpOperator.SMALLER_THAN));
		put("and", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.AND));
		put("or", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.OR));
		put("not", new UnaryVMCommand(UnaryVMCommand.UnaryOperator.NOT));
		put("return", new FunctionReturnCommand());

	}};


	private final BufferedReader reader;

	private String line;

	public VMParser(File file) throws FileNotFoundException {

		reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
	}

	public boolean moveToNext() throws IOException {

		while (true) {

			line = reader.readLine();

			if (line == null) {
				return false;
			}

			line = line.trim();

			if (line.isEmpty()) {
				continue;
			}

			Matcher m = commentPattern.matcher(line);
			if (m.matches()) {
				continue;
			}

			// remove comment from line
			if (m.find()) {
				line = line.substring(0, m.start());
				line = line.trim();
			}

			return true;

		}

	}

	public VMCommand get() throws VMFileException {

		VMCommand command = parseTable(line);
		if (command != null) {
			return command;
		}

		command = parseMemoryAccessCommand(line);
		if (command != null) {
			return command;
		}

		command = parseProgramFlow(line);
		if (command != null) {
			return command;
		}

		command = parseFunction(line);
		if (command != null) {
			return command;
		}

		throw new VMFileException("Couldn't parse: " + line);

	}

	private VMCommand parseFunction(String line) {

		String[] arr = line.split(" ");

		if (arr.length != 3) {
			return null;
		}

		String functionName = arr[1];
		int arguments;
		try {
			arguments = Integer.valueOf(arr[2]);
		} catch (NumberFormatException e) {
			return null;
		}

		switch (arr[0]) {
			case FUNCTION:
				return new FunctionDecelerationCommand(functionName, arguments);
			case CALL:
				return new FunctionCallCommand(functionName, arguments);
		}

		return null;
	}

	private VMCommand parseProgramFlow(String line) {

		String[] arr = line.split(" ");

		if (arr.length != 2) {
			return null;
		}

		String label = arr[1];
		switch (arr[0]) {
			case LABEL:
				return new LabelCommand(label);
			case GOTO:
				return new GotoCommand(label);
			case IF_GOTO:
				return new IfGotoCommand(label);
		}

		return null;
	}

	private VMCommand parseMemoryAccessCommand(String line) {

		String[] arr = line.split(" ");

		if (arr.length != 3) {
			return null;
		}

		if (arr[0].equals(POP)) {
			return new PopVMCommand(Segment.getSegment(arr[1]), arr[2]);
		}

		if (arr[0].equals(PUSH)) {
			return new PushVMCommand(Segment.getSegment(arr[1]), arr[2]);
		}

		return null;
	}

	private static VMCommand parseTable(String line) {

		return table.getOrDefault(line, null);

	}

	@Override
	public void close() throws Exception {

		reader.close();

	}
}
