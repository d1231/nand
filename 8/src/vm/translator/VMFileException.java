package vm.translator;

/**
 * Created by danny on 22/03/15.
 */
public class VMFileException extends Exception {

	public VMFileException(String message) {

		super(message);
	}
}
