// This file is part of the materials accompanying the book 
// "The Elements of Computing Systems" by Nisan and Schocken, 
// MIT Press. Book site: www.idc.ac.il/tecs
// File name: projects/08/ProgramFlow/FibonacciSeries/FibonacciSeriesVME.tst

load Main.vm,
output-file output.out,
output-list RAM[8000]%D1.6.2 RAM[8001]%D1.6.2 RAM[8002]%D1.6.2 
            RAM[3003]%D1.6.2 RAM[3004]%D1.6.2 RAM[3005]%D1.6.2;

set RAM[8000] 12320,

repeat 5555555 {
  vmstep;
}

output;
