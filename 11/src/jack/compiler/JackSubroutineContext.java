package jack.compiler;

import jack.compiler.jack_class.SymbolTable;

/**
 * Created by danny on 23/04/15.
 */
public class JackSubroutineContext {

	private final String returnType;

	private final SymbolTable symbolTable;

	private final JackClassContext classContext;

	private int count = 0;

	public JackSubroutineContext(String returnType, SymbolTable symbolTable, JackClassContext classContext) {

		this.returnType = returnType;
		this.symbolTable = symbolTable;
		this.classContext = classContext;
	}

	public String getReturnType() {

		return returnType;
	}

	public String getFreeLabel() {

		return String.format("l%d", count++);

	}

	public SymbolTable.Value get(String value) {

		SymbolTable.Value retVal = symbolTable.get(value);
		if (retVal == null) {
			retVal = getClassContext().getSymbolTable().get(value);
		}

		return retVal;
	}

	public JackClassContext getClassContext() {

		return classContext;
	}
}
