package jack.compiler.subroutine.body;

import jack.compiler.JackElement;
import jack.compiler.statements.JackStatements;
import jack.compiler.variable_deceleration.SubroutineVariableDeceleration;

import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineBody implements JackElement {


	private final ArrayList<SubroutineVariableDeceleration> subRoutineVarDec;

	private final JackStatements statements;

	public SubroutineBody(ArrayList<SubroutineVariableDeceleration> subRoutineVarDec, JackStatements statements) {

		this.subRoutineVarDec = subRoutineVarDec;
		this.statements = statements;

	}

	public ArrayList<SubroutineVariableDeceleration> getVarDec() {

		return subRoutineVarDec;
	}

	public JackStatements getStatements() {

		return statements;
	}

	@Override
	public String toString() {

		return "SubroutineBody{" +
				"subRoutineVarDec=" + subRoutineVarDec +
				", statements=" + statements +
				'}';
	}

}
