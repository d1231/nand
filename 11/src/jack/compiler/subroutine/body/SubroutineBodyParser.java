package jack.compiler.subroutine.body;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.statements.JackStatements;
import jack.compiler.statements.parser.JackStatementsParser;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.utils.JackUtils;
import jack.compiler.variable_deceleration.SubroutineVariableDeceleration;
import jack.compiler.variable_deceleration.parser.SubroutineVariableDecelerationParser;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineBodyParser {

	public static SubroutineBody parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!JackUtils.isOpeningCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new SubroutineBodyParserException();
		}

		ArrayList<SubroutineVariableDeceleration> subRoutineVarDec = new ArrayList<>();

		SubroutineVariableDeceleration varDec;
		while ((varDec = SubroutineVariableDecelerationParser.parse(jackTokenFeeder)) != null) {
			subRoutineVarDec.add(varDec);
		}

		JackStatements statements = JackStatementsParser.parse(jackTokenFeeder);

		if (!JackUtils.isClosingCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new SubroutineBodyParserException();
		}

		return new SubroutineBody(subRoutineVarDec, statements);
	}

}
