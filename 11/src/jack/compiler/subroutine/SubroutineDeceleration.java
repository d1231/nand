package jack.compiler.subroutine;

import jack.compiler.JackClassContext;
import jack.compiler.JackElement;
import jack.compiler.JackSubroutineContext;
import jack.compiler.jack_class.SymbolTable;
import jack.compiler.statements.JackStatement;
import jack.compiler.subroutine.body.SubroutineBody;
import jack.compiler.subroutine.parameter_list.SubroutineParameterList;
import jack.compiler.tokenizer.token.JackKeyword;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.variable_deceleration.SubroutineVariableDeceleration;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineDeceleration implements JackElement {

	static final String CONSTRUCTOR = "constructor";

	private static final String METHOD = "method";

	private static final String MEMORY_ALLOC = "Memory.alloc";

	private final JackToken type;

	private final JackToken returnType;

	private final JackToken subroutineName;

	private final SubroutineParameterList parameterList;

	private final SubroutineBody subroutineBody;

	private final SymbolTable symbolTable;

	private final int arguments;

	public SubroutineDeceleration(JackToken type, JackToken returnType, JackToken subroutineName,
			SubroutineParameterList parameterList, SubroutineBody subroutineBody) {

		this.type = type;
		this.returnType = returnType;
		this.subroutineName = subroutineName;
		this.parameterList = parameterList;
		this.subroutineBody = subroutineBody;

		final ArrayList<SubroutineVariableDeceleration> varDec = subroutineBody.getVarDec();

		int argumentsStart = type.equals(JackKeyword.factory("method")) ? 1 : 0;

		symbolTable = SymbolTable.newInstance(varDec, argumentsStart, parameterList);


		this.arguments = symbolTable.size() - parameterList.size();
	}


	@Override
	public String toString() {

		return "SubroutineDeceleration{" +
				"type=" + type +
				", returnType=" + returnType +
				", subroutineName=" + subroutineName +
				", parameterList=" + parameterList +
				", subroutineBody=" + subroutineBody +
				'}';
	}

	public void writeVM(JackVMWriter writer, JackClassContext context) throws IOException {

		int numOfArg;

		if (type.equals(JackKeyword.factory(METHOD))) {
			numOfArg = this.arguments + 1;
		} else {
			numOfArg = this.arguments;
		}

		writer.writeFunction(String.format("%s.%s", context.getFileName(), subroutineName.getValue()), numOfArg);

		if (type.equals(JackKeyword.factory(CONSTRUCTOR))) {

			writer.writePush(Segment.CONSTANT, context.getSize());
			writer.writeCall(MEMORY_ALLOC, 1);
			writer.writePop(Segment.POINTER, 0);

		} else if (type.equals(JackKeyword.factory("method"))) {

			writer.writePush(Segment.ARGUMENT, 0);
			writer.writePop(Segment.POINTER, 0);
		}

		JackSubroutineContext subroutineContext = new JackSubroutineContext(returnType.getValue(), symbolTable,
				context);

		for (JackStatement jackStatement : subroutineBody.getStatements()) {

			jackStatement.writeVM(writer, subroutineContext);
		}

		writer.nextLine();
	}
}
