package jack.compiler.subroutine.parameter_list;

import jack.compiler.JackElement;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.Tuple;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineParameterList implements JackElement, Iterable<Tuple<JackToken, JackToken>> {

	private final ArrayList<Tuple<JackToken, JackToken>> params;

	public SubroutineParameterList(ArrayList<Tuple<JackToken, JackToken>> params) {

		this.params = params;

	}

	@Override
	public String toString() {

		return "SubroutineParameterList{" +
				"params=" + params +
				'}';
	}

	@Override
	public Iterator<Tuple<JackToken, JackToken>> iterator() {

		return params.iterator();
	}

	public int size() {

		return params.size();
	}
}
