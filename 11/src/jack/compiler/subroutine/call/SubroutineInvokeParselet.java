package jack.compiler.subroutine.call;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.parser.JackTermParselet;
import jack.compiler.subroutine.call.expression_list.ExpressionList;
import jack.compiler.subroutine.call.expression_list.ExpressionListParselet;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class SubroutineInvokeParselet implements JackTermParselet {

	private boolean isPartOfExpression;

	public SubroutineInvokeParselet(boolean isPartOfExpression) {

		this.isPartOfExpression = isPartOfExpression;
	}

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {


		JackToken inkovee = jackTokenFeeder.consumeNext();

		if (!JackUtils.isPoint(jackTokenFeeder.consumeNext())) {
			throw new JackSubroutineCallParseletException();
		}

		JackToken name = jackTokenFeeder.consumeNext();

		if (!JackUtils.isOpeningBracket(jackTokenFeeder.consumeNext())) {
			throw new JackSubroutineCallParseletException();
		}

		ExpressionList expressionList = ExpressionListParselet.parse(jackTokenFeeder);

		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new JackSubroutineCallParseletException();
		}


		return new SubroutineInvoke(inkovee, name, expressionList, isPartOfExpression);
	}
}
