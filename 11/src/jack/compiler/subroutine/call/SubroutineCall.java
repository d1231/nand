package jack.compiler.subroutine.call;

import jack.compiler.JackSubroutineContext;
import jack.compiler.expression.JackExpression;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.subroutine.call.expression_list.ExpressionList;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class SubroutineCall extends JackTerm {

	protected final JackToken subroutineName;

	private final ExpressionList expressionList;

	private final boolean isPartOfExpression;

	public SubroutineCall(JackToken name, ExpressionList expressionList, boolean isPartOfExpression) {

		subroutineName = name;
		this.expressionList = expressionList;
		this.isPartOfExpression = isPartOfExpression;
	}

	@Override
	public String toString() {

		return "SubroutineCall{" +
				"subroutineName=" + subroutineName +
				", expressionList=" + expressionList +
				", isPartOfExpression=" + isPartOfExpression +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		// without invokee, then calling method on this object

		writeThis(writer);

		for (JackExpression jackExpression : expressionList) {
			jackExpression.writeVM(writer, subroutineContext);
		}

		int arguments = getArgumentNumber();

		writer.writeCall(getSubroutineVMName(subroutineContext), arguments);

	}

	protected int getArgumentNumber() {

		return expressionList.size() + 1;
	}

	protected void writeThis(JackVMWriter writer) throws IOException {

		writer.writePush(Segment.POINTER, 0); // push this
	}

	protected String getSubroutineVMName(JackSubroutineContext subroutineContext) {

		return String.format("%s.%s", subroutineContext.getClassContext().getFileName(), subroutineName.getValue());
	}
}
