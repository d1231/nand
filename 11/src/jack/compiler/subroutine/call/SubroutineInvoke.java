package jack.compiler.subroutine.call;

import jack.compiler.JackSubroutineContext;
import jack.compiler.jack_class.SymbolTable;
import jack.compiler.subroutine.call.expression_list.ExpressionList;
import jack.compiler.tokenizer.token.JackSymbol;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.vm.JackVMWriter;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class SubroutineInvoke extends SubroutineCall {

	private final static JackSymbol point = JackSymbol.factory('.');

	private final JackToken invokee;

	private SymbolTable.Value var;

	public SubroutineInvoke(JackToken inkovee, JackToken name, ExpressionList expressionList, boolean
			isPartOfExpression) {

		super(name, expressionList, isPartOfExpression);
		this.invokee = inkovee;
	}

	@Override
	public String toString() {

		return "SubroutineInvoke{" +
				"invokee=" + invokee +
				"}, " + super.toString();
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		var = subroutineContext.get(invokee.getValue());

		super.writeVM(writer, subroutineContext);
	}

	@Override
	protected int getArgumentNumber() {

		if (var == null) {
			return super.getArgumentNumber() - 1;
		} else {
			return super.getArgumentNumber();
		}
	}

	@Override
	protected void writeThis(JackVMWriter writer) throws IOException {

		if (var != null) {

			writer.writePush(var.segment, var.index);

		}
	}

	@Override
	protected String getSubroutineVMName(JackSubroutineContext subroutineContext) {

		String prefix;

		if (var == null) {

			prefix = invokee.getValue();
		} else {

			prefix = var.type;
		}

		return String.format("%s.%s", prefix, subroutineName.getValue());
	}
}
