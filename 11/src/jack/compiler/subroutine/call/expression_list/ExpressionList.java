package jack.compiler.subroutine.call.expression_list;

import jack.compiler.JackElement;
import jack.compiler.expression.JackExpression;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by danny on 19/04/15.
 */
public class ExpressionList implements JackElement, Iterable<JackExpression> {

	private final ArrayList<JackExpression> expressions;

	public ExpressionList(ArrayList<JackExpression> expressions) {

		this.expressions = expressions;

	}

	@Override
	public String toString() {

		return "ExpressionList{" +
				"expressions=" + expressions +
				'}';
	}


	@Override
	public Iterator<JackExpression> iterator() {

		return expressions.iterator();
	}

	public int size() {

		return expressions.size();
	}
}
