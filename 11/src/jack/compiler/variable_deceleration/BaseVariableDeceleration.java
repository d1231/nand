package jack.compiler.variable_deceleration;

import jack.compiler.JackElement;
import jack.compiler.tokenizer.token.JackToken;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by danny on 18/04/15.
 */
public class BaseVariableDeceleration implements JackElement {


	private final JackToken type;

	private final ArrayList<JackToken> varNames;

	public BaseVariableDeceleration(JackToken type, ArrayList<JackToken> varNames) {

		this.type = type;
		this.varNames = varNames;
	}

	@Override
	public String toString() {

		return "BaseVariableDeceleration{" +
				"type=" + type +
				", varNames=" + varNames +
				'}';
	}

	public String getType() {

		return type.getValue();
	}

	public List<String> getNames() {

		return varNames.stream().map(JackToken::getValue).collect(Collectors.toList());
	}

	public int getSize() {

		return varNames.size();
	}
}
