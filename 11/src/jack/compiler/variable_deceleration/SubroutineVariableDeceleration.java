package jack.compiler.variable_deceleration;

import jack.compiler.JackElement;
import jack.compiler.tokenizer.token.JackToken;

import java.util.List;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineVariableDeceleration implements JackElement {

	private final JackToken type;

	private final BaseVariableDeceleration variableDeceleration;


	public SubroutineVariableDeceleration(JackToken type, BaseVariableDeceleration variableDeceleration) {

		this.type = type;
		this.variableDeceleration = variableDeceleration;
	}

	@Override
	public String toString() {

		return "SubroutineVariableDeceleration{" +
				"type=" + type +
				", variableDeceleration=" + variableDeceleration +
				'}';
	}

	public String getType() {

		return variableDeceleration.getType();
	}

	public List<String> getNames() {

		return variableDeceleration.getNames();
	}
}
