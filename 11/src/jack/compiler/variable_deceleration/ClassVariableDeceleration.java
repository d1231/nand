package jack.compiler.variable_deceleration;

import jack.compiler.JackElement;
import jack.compiler.tokenizer.token.JackKeyword;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.vm.Segment;

import java.util.List;

/**
 * Created by danny on 18/04/15.
 */
public class ClassVariableDeceleration implements JackElement {

	private final JackToken type;

	private final BaseVariableDeceleration variableDeceleration;


	public ClassVariableDeceleration(JackToken type, BaseVariableDeceleration variableDeceleration) {

		this.type = type;
		this.variableDeceleration = variableDeceleration;
	}

	public Segment getSegment() {

		if (type.equals(JackKeyword.factory("field"))) {
			return Segment.THIS;
		} else {
			return Segment.STATIC;
		}
	}

	public List<String> getNames() {

		return variableDeceleration.getNames();
	}

	public String getType() {

		return variableDeceleration.getType();
	}

	public String geClassVarType() {

		return type.getValue();
	}

	@Override
	public String toString() {

		return "ClassVariableDeceleration{" +
				"type=" + type +
				", variableDeceleration=" + variableDeceleration +
				'}';
	}

	public int getSize() {

		return variableDeceleration.getSize();
	}
}
