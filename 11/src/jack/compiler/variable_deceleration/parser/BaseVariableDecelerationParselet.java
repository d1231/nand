package jack.compiler.variable_deceleration.parser;

import jack.compiler.JackTokenFeeder;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.JackUtils;
import jack.compiler.variable_deceleration.BaseVariableDeceleration;
import jack.compiler.variable_deceleration.VariableDecelerationException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 17/04/15.
 */
public class BaseVariableDecelerationParselet {

	public static BaseVariableDeceleration parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, VariableDecelerationException {

		VariableDecelerationBuilder builder = new VariableDecelerationBuilder();

		builder.setType(jackTokenFeeder.consumeNext());

		builder.setVarName(jackTokenFeeder.consumeNext());

		JackToken token = jackTokenFeeder.consumeNext();

		while (!JackUtils.isEndToken(token)) {

			builder.addVarName(token, jackTokenFeeder.consumeNext());

			token = jackTokenFeeder.consumeNext();
		}

		return builder.build(token);

	}

	/**
	 * Created by danny on 17/04/15.
	 */
	private static class VariableDecelerationBuilder {


		private JackToken type;


		private ArrayList<JackToken> varNames = new ArrayList<>();


		public void setType(JackToken jackToken) throws VariableDecelerationException {

			if (!JackUtils.isType(jackToken)) {
				throw new VariableDecelerationException();
			}

			this.type = jackToken;

		}

		public void setVarName(JackToken jackToken) throws VariableDecelerationException {

			if (!JackUtils.isValidName(jackToken)) {
				throw new VariableDecelerationException();
			}

			this.varNames.add(jackToken);
		}

		public void addVarName(JackToken separator, JackToken varName) throws VariableDecelerationException {


			if (!JackUtils.isSeparator(separator)) {
				throw new VariableDecelerationException();
			}

			if (!JackUtils.isValidName(varName)) {
				throw new VariableDecelerationException();
			}

			varNames.add(varName);
		}

		public BaseVariableDeceleration build(JackToken end) throws VariableDecelerationException {

			if (!JackUtils.isEndToken(end)) {
				throw new VariableDecelerationException();
			}

			return new BaseVariableDeceleration(type, varNames);
		}
	}
}
