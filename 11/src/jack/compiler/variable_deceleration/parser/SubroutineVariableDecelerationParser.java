package jack.compiler.variable_deceleration.parser;

import jack.compiler.JackTokenFeeder;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.tokenizer.token.JackTokenType;
import jack.compiler.variable_deceleration.BaseVariableDeceleration;
import jack.compiler.variable_deceleration.SubroutineVariableDeceleration;
import jack.compiler.variable_deceleration.VariableDecelerationException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danny on 18/04/15.
 */
public class SubroutineVariableDecelerationParser {

	public static SubroutineVariableDeceleration parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, VariableDecelerationException {

		ClassVariableDecelerationBuilder builder = new ClassVariableDecelerationBuilder();

		if (!builder.setType(jackTokenFeeder.peekNext())) {
			return null;
		}

		jackTokenFeeder.consumeNext();

		BaseVariableDeceleration variableDeceleration = BaseVariableDecelerationParselet.parse(jackTokenFeeder);

		builder.setVariableDeceleration(variableDeceleration);

		return builder.build();

	}

	private static class ClassVariableDecelerationBuilder {

		private final static Set<String> types = new HashSet<String>() {{
			add("var");
		}};

		private JackToken type;

		private BaseVariableDeceleration variableDeceleration;

		public boolean setType(JackToken jackToken) {

			if (jackToken.getTokenType().equals(JackTokenType.KEYWORD) && types.contains(jackToken.getValue())) {
				type = jackToken;
				return true;
			} else {
				return false;
			}
		}

		public void setVariableDeceleration(BaseVariableDeceleration variableDeceleration) {

			this.variableDeceleration = variableDeceleration;
		}

		public SubroutineVariableDeceleration build() {

			return new SubroutineVariableDeceleration(type, variableDeceleration);

		}
	}

}
