package jack.compiler.utils;

import jack.compiler.vm.ArithmeticVMCommand;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.tokenizer.token.JackSymbol;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.tokenizer.token.JackTokenType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danny on 17/04/15.
 */
public class JackUtils {

	public static final String JACK_SUFFIX = ".jack";

	private final static Set<String> types = new HashSet<String>() {{
		add("int");
		add("boolean");
		add("char");
	}};

	private static final JackToken endToken = JackSymbol.factory(';');

	private static final JackToken separatorToken = JackSymbol.factory(',');

	private static final JackToken openingBracketToken = JackSymbol.factory('(');

	private static final JackToken closingBracketToken = JackSymbol.factory(')');

	private static final JackToken openingCurlyBracketToken = JackSymbol.factory('{');

	private static final JackToken closingCurlyBracketToken = JackSymbol.factory('}');

	private static final JackToken openingSquareBracket = JackSymbol.factory('[');

	private static final JackToken closingSquareBracket = JackSymbol.factory(']');

	private static final JackToken point = JackSymbol.factory('.');

	private static final JackToken equalSign = JackSymbol.factory('=');

	private static final String MATH_MULTIPLY = "Math.multiply";

	private static final String MATH_DIVIDE = "Math.divide";

	public static boolean isEndToken(JackToken token) {

		return endToken.equals(token);
	}

	public static boolean isType(JackToken jackToken) {

		return (JackTokenType.KEYWORD.equals(jackToken.getTokenType()) && types.contains(jackToken.getValue())) ||
				JackTokenType.IDENTIFIER.equals(jackToken.getTokenType());
	}

	public static boolean isSeparator(JackToken separator) {

		return separatorToken.equals(separator);
	}

	public static boolean isValidName(JackToken jackToken) {

		return jackToken != null && JackTokenType.IDENTIFIER.equals(jackToken.getTokenType());
	}

	public static boolean isClosingBracket(JackToken closingBracket) {

		return closingBracketToken.equals(closingBracket);
	}

	public static boolean isOpeningBracket(JackToken jackToken) {

		return openingBracketToken.equals(jackToken);
	}

	public static boolean isClosingCurlyBracket(JackToken closingBracket) {

		return closingCurlyBracketToken.equals(closingBracket);
	}

	public static boolean isOpeningCurlyBracket(JackToken jackToken) {

		return openingCurlyBracketToken.equals(jackToken);
	}

	public static boolean isOpeningSquareBracket(JackToken jackToken) {

		return openingSquareBracket.equals(jackToken);
	}

	public static boolean isClosingSquareBracket(JackToken jackToken) {

		return closingSquareBracket.equals(jackToken);
	}

	public static boolean isPoint(JackToken jackToken) {

		return point.equals(jackToken);
	}

	public static boolean isEqualSign(JackToken jackToken) {

		return equalSign.equals(jackToken);
	}


	public static void writeBinaryOp(JackVMWriter writer, JackToken op) throws IOException {

		switch (op.getValue()) {
			case "+":
				writer.writeArithmetic(ArithmeticVMCommand.ADD);
				break;
			case "-":
				writer.writeArithmetic(ArithmeticVMCommand.SUB);
				break;
			case "&amp;":
				writer.writeArithmetic(ArithmeticVMCommand.AND);
				break;
			case "*":
				writer.writeCall(MATH_MULTIPLY, 2);
				break;
			case "/":
				writer.writeCall(MATH_DIVIDE, 2);
				break;
			case "|":
				writer.writeArithmetic(ArithmeticVMCommand.OR);
				break;
			case "&lt;":
				writer.writeArithmetic(ArithmeticVMCommand.LT);
				break;
			case "=":
				writer.writeArithmetic(ArithmeticVMCommand.EQ);
				break;
			case "&gt;":
				writer.writeArithmetic(ArithmeticVMCommand.GT);
				break;
			case "~":
				writer.writeArithmetic(ArithmeticVMCommand.NEG);
				break;
		}

	}
	public static void writeUnaryOp(JackVMWriter writer, JackToken op) throws IOException {

		switch (op.getValue()) {
			case "-":
				writer.writeArithmetic(ArithmeticVMCommand.NEG);
				break;
			case "~":
				writer.writeArithmetic(ArithmeticVMCommand.NOT);
				break;
		}

	}
}
