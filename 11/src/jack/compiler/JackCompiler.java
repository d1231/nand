package jack.compiler;

import jack.compiler.jack_class.JackClass;
import jack.compiler.tokenizer.JackTokenizer;
import jack.compiler.utils.JackUtils;
import jack.compiler.vm.JackVMWriter;

import java.io.File;

/**
 * Created by danny on 10/04/15.
 */
public class JackCompiler {

	private final File file;

	private JackTokenizer tokenizer;

	public JackCompiler(File file) {

		this.file = file;


	}

	public void compile() throws Exception {

		tokenizer = new JackTokenizer(file);
		JackComplicationEngine complicationEngine = new JackComplicationEngine(tokenizer);

		JackClass jackClass = complicationEngine.createClass();

		try (JackVMWriter writer = new JackVMWriter(getOutputFile())) {

			jackClass.writeVM(writer);
		}


	}

	private File getOutputFile() {

		// TODO better
		// case of file like idiot/.jack/file.jack

		return new File(file.getAbsolutePath().replace(JackUtils.JACK_SUFFIX, ".vm"));
	}


}
