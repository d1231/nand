package jack.compiler.expression.term;

import jack.compiler.JackSubroutineContext;
import jack.compiler.expression.JackExpression;
import jack.compiler.jack_class.SymbolTable;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.vm.ArithmeticVMCommand;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class JackArrayTerm extends JackTerm {

	private final JackToken name;

	private final JackExpression expression;

	public JackArrayTerm(JackToken name, JackExpression expression) {

		this.name = name;
		this.expression = expression;
	}


	@Override
	public String toString() {

		return "JackArrayTerm{" +
				"name=" + name +
				", expression=" + expression +
				'}';
	}


	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		SymbolTable.Value var = subroutineContext.get(name.getValue());
		writer.writePush(var.segment, var.index);

		expression.writeVM(writer, subroutineContext);
		writer.writeArithmetic(ArithmeticVMCommand.ADD);
		writer.writePop(Segment.POINTER, 1);
		writer.writePush(Segment.THAT, 0);

	}
}
