package jack.compiler.expression.term;


import jack.compiler.JackElement;
import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public abstract class JackTerm implements JackElement {

	public abstract void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException;
}
