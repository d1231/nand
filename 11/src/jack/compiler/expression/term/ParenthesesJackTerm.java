package jack.compiler.expression.term;

import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.expression.JackExpression;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class ParenthesesJackTerm extends JackTerm {

	private final JackExpression expression;

	public ParenthesesJackTerm(JackExpression expression) {

		this.expression = expression;
	}


	@Override
	public String toString() {

		return "ParenthesesJackTerm{" +
				"expression=" + expression +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		expression.writeVM(writer, subroutineContext);

	}
}
