package jack.compiler.expression.term;

import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermWrapper extends JackTerm {

	private final JackToken wrapped;

	public JackTermWrapper(JackToken jackToken) {

		this.wrapped = jackToken;
	}

	@Override
	public String toString() {

		return "JackTermWrapper{" +
				"wrapped=" + wrapped +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		wrapped.writeVM(writer, subroutineContext);
	}
}
