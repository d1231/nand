package jack.compiler.expression.term.parser;

import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.JackTermWrapper;
import jack.compiler.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermStringConstantParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException {

		return new JackTermWrapper(jackTokenFeeder.consumeNext());
	}
}
