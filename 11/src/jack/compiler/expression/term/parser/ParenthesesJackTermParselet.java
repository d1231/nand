package jack.compiler.expression.term.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.JackExpression;
import jack.compiler.expression.JackExpressionParser;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.ParenthesesJackTerm;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class ParenthesesJackTermParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		jackTokenFeeder.consumeNext(); // consume '('

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		// consume ')'
		if (!JackUtils.isClosingBracket(jackTokenFeeder.consumeNext())) {
			throw new JackTermParserException();
		}

		return new ParenthesesJackTerm(expression);
	}
}
