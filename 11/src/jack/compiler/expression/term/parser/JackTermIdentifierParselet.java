package jack.compiler.expression.term.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.JackTermWrapper;
import jack.compiler.subroutine.call.SubRoutineCallParselet;
import jack.compiler.subroutine.call.SubroutineInvokeParselet;
import jack.compiler.tokenizer.JackTokenizerException;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermIdentifierParselet implements JackTermParselet {

	private final static HashMap<String, JackTermParselet> parselets = new HashMap<String, JackTermParselet>() {{

		put("[", new JackArrayTermParselet());
		put(".", new SubroutineInvokeParselet(true));
		put("(", new SubRoutineCallParselet(true));

	}};


	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		JackTermParselet parselet = parselets.get(jackTokenFeeder.peekNextNext().getValue());

		if (parselet == null) {
			return new JackTermWrapper(jackTokenFeeder.consumeNext());
		} else {
			return parselet.parse(jackTokenFeeder);
		}

	}

}
