package jack.compiler.expression.term.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermSymbolParselet implements JackTermParselet {

	private final static HashMap<String, JackTermParselet> parselets = new HashMap<String, JackTermParselet>() {{

		put("~", new UnaryJackTermParselet());
		put("-", new UnaryJackTermParselet());
		put("(", new ParenthesesJackTermParselet());

	}};

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {


		JackToken token = jackTokenFeeder.peekNext();

		JackTermParselet parselet = parselets.get(token.getValue());

		if (parselet == null) {
			return null;
		} else {
			return parselet.parse(jackTokenFeeder);
		}
	}

}
