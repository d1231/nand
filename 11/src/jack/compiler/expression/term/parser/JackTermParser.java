package jack.compiler.expression.term.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.tokenizer.token.JackTokenType;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermParser {

	private final static HashMap<JackTokenType, JackTermParselet> parselets = new HashMap<JackTokenType,
			JackTermParselet>() {{

		put(JackTokenType.IDENTIFIER, new JackTermIdentifierParselet());
		put(JackTokenType.SYMBOL, new JackTermSymbolParselet());
		put(JackTokenType.INTEGER_CONSTANT, new JackTermIntegerConstantParselet());
		put(JackTokenType.STRING_CONSTANT, new JackTermStringConstantParselet());
		put(JackTokenType.KEYWORD, new JackTermKeywordParselet());

	}};

	public static JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {


		JackToken token = jackTokenFeeder.peekNext();

		return parselets.get(token.getTokenType()).parse(jackTokenFeeder);

	}
}
