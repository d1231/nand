package jack.compiler.expression.term.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.UnaryJackTerm;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class UnaryJackTermParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		JackToken op = jackTokenFeeder.consumeNext();
		JackTerm unaryTerm = JackTermParser.parse(jackTokenFeeder);

		return new UnaryJackTerm(op, unaryTerm);
	}
}
