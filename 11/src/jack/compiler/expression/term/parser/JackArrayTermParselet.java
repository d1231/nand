package jack.compiler.expression.term.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.JackExpression;
import jack.compiler.expression.JackExpressionParser;
import jack.compiler.expression.term.JackArrayTerm;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class JackArrayTermParselet implements JackTermParselet {

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		JackToken name = jackTokenFeeder.consumeNext();

		// consume '['
		if (!JackUtils.isOpeningSquareBracket(jackTokenFeeder.consumeNext())) {
			throw new JackTermParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		// consume ']'
		if (!JackUtils.isClosingSquareBracket(jackTokenFeeder.consumeNext())) {
			throw new JackTermParserException();
		}

		return new JackArrayTerm(name, expression);
	}

}
