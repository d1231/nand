package jack.compiler.expression.term.parser;

import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.JackTermWrapper;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danny on 18/04/15.
 */
public class JackTermKeywordParselet implements JackTermParselet {

	private final static Set<String> constants = new HashSet<String>() {{

		add("true");
		add("false");
		add("null");
		add("this");

	}};

	@Override
	public JackTerm parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackTermParserException {

		JackToken term = jackTokenFeeder.consumeNext();

		if (!isKeyboardConstant(term.getValue())) {
			throw new JackTermParserException();
		}

		return new JackTermWrapper(term);
	}

	private static boolean isKeyboardConstant(String term) {

		return constants.contains(term);
	}
}
