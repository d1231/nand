package jack.compiler.expression.term;

import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class UnaryJackTerm extends JackTerm {

	private final JackTerm unaryTerm;

	private final JackToken op;

	public UnaryJackTerm(JackToken op, JackTerm unaryTerm) {

		this.op = op;
		this.unaryTerm = unaryTerm;

	}


	@Override
	public String toString() {

		return "UnaryJackTerm{" +
				"unaryTerm=" + unaryTerm +
				", op=" + op +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		unaryTerm.writeVM(writer, subroutineContext);

		JackUtils.writeUnaryOp(writer, op);
	}
}
