package jack.compiler.expression;

import jack.compiler.JackElement;
import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.JackUtils;
import jack.compiler.utils.Tuple;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 17/04/15.
 */
public class JackExpression implements JackElement {

	private final JackTerm term;

	private final ArrayList<Tuple<JackToken, JackTerm>> additionalTerms;

	public JackExpression(JackTerm term, ArrayList<Tuple<JackToken, JackTerm>> additionalTerms) {

		this.term = term;
		this.additionalTerms = additionalTerms;

	}

	@Override
	public String toString() {

		return "JackExpression{" +
				"term=" + term +
				", additionalTerms=" + additionalTerms +
				'}';
	}

	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		term.writeVM(writer, subroutineContext);

		for (Tuple<JackToken, JackTerm> additionalTerm : additionalTerms) {
			JackToken op = additionalTerm.x;

			additionalTerm.y.writeVM(writer, subroutineContext);

			JackUtils.writeBinaryOp(writer, op);
		}

	}
}
