package jack.compiler.jack_class;

import jack.compiler.JackClassContext;
import jack.compiler.JackElement;
import jack.compiler.subroutine.SubroutineDeceleration;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.variable_deceleration.ClassVariableDeceleration;
import jack.compiler.vm.JackVMWriter;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 19/04/15.
 */
public class JackClass implements JackElement {

	private final JackToken className;

	private final ArrayList<ClassVariableDeceleration> classVariableDecelerations;

	private final ArrayList<SubroutineDeceleration> subroutineDecelerations;

	private final SymbolTable symbolTable;

	private JackClassContext context;

	public JackClass(JackToken className, ArrayList<ClassVariableDeceleration> classVariableDecelerations,
			ArrayList<SubroutineDeceleration> subroutineDecelerations) {

		this.className = className;
		this.classVariableDecelerations = classVariableDecelerations;
		this.subroutineDecelerations = subroutineDecelerations;


		symbolTable = SymbolTable.newInstance(classVariableDecelerations);

		int count = 0;
		for (ClassVariableDeceleration classVariableDeceleration : classVariableDecelerations) {

			final String type = classVariableDeceleration.geClassVarType();
			if (type.equals("field")) {

				count += classVariableDeceleration.getNames().size();
			}

		}

		context = new JackClassContext(symbolTable, className.getValue(), count);

	}

	public void writeVM(JackVMWriter writer) throws IOException {

		for (SubroutineDeceleration subroutineDeceleration : subroutineDecelerations) {

			subroutineDeceleration.writeVM(writer, context);

		}
	}

}
