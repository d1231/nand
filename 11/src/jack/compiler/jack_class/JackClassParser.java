package jack.compiler.jack_class;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.subroutine.SubroutineDeceleration;
import jack.compiler.subroutine.SubroutineDecelerationParser;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackKeyword;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.tokenizer.token.JackTokenType;
import jack.compiler.utils.JackUtils;
import jack.compiler.variable_deceleration.ClassVariableDeceleration;
import jack.compiler.variable_deceleration.parser.ClassVariableDecelerationParselet;
import jack.compiler.variable_deceleration.VariableDecelerationException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danny on 17/04/15.
 */
public class JackClassParser {


	private static final JackKeyword CLASS = JackKeyword.factory("class");

	public static JackClass parse(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {


		JackClassBuilder jackClassBuilder = new JackClassBuilder();

		// get class keyword token
		if (!CLASS.equals(jackTokenFeeder.consumeNext())) {
			throw new JackClassParserException();
		}

		// get class name
		jackClassBuilder.setClassName(jackTokenFeeder.consumeNext());

		// get symbol '{'
		if (!JackUtils.isOpeningCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new JackClassParserException();
		}

		parseVariablesDeceleration(jackClassBuilder, jackTokenFeeder);

		parseSubroutineDec(jackClassBuilder, jackTokenFeeder);

		// get symbol '{'
		if (!JackUtils.isClosingCurlyBracket(jackTokenFeeder.consumeNext())) {
			throw new JackClassParserException();
		}

		return jackClassBuilder.build();
	}

	private static void parseSubroutineDec(JackClassBuilder builder, JackTokenFeeder jackTokenFeeder) throws
			JackParserException, JackTokenizerException, IOException {

		SubroutineDeceleration subroutineDeceleration;
		while ((subroutineDeceleration = SubroutineDecelerationParser.parse(jackTokenFeeder)) != null) {

			builder.addVarDec(subroutineDeceleration);
		}


	}

	private static void parseVariablesDeceleration(JackClassBuilder builder, JackTokenFeeder jackTokenFeeder) throws
			IOException, JackTokenizerException, VariableDecelerationException {

		ClassVariableDeceleration varDec;
		while ((varDec = ClassVariableDecelerationParselet.parse(jackTokenFeeder)) != null) {

			builder.addVarDec(varDec);
		}

	}


	/**
	 * Created by danny on 17/04/15.
	 */
	public static class JackClassBuilder {

		public static final String CLASS = "class";


		private JackToken className;

		private ArrayList<ClassVariableDeceleration> classVariableDecelerations = new ArrayList<>();

		private ArrayList<SubroutineDeceleration> subroutineDecelerations = new ArrayList<>();


		public void setClassName(JackToken identifier) throws JackClassParserException {

			if (!identifier.getTokenType().equals(JackTokenType.IDENTIFIER)) {
				throw new JackClassParserException();
			}

			this.className = identifier;

		}

		public void addVarDec(ClassVariableDeceleration varDec) {

			classVariableDecelerations.add(varDec);
		}

		public void addVarDec(SubroutineDeceleration subroutineDeceleration) {

			subroutineDecelerations.add(subroutineDeceleration);

		}

		public JackClass build() {

			return new JackClass(className, classVariableDecelerations, subroutineDecelerations);
		}
	}
}
