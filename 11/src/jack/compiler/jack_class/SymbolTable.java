package jack.compiler.jack_class;

import jack.compiler.subroutine.parameter_list.SubroutineParameterList;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.variable_deceleration.ClassVariableDeceleration;
import jack.compiler.variable_deceleration.SubroutineVariableDeceleration;
import jack.compiler.vm.Segment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by danny on 23/04/15.
 */
public class SymbolTable {

	HashMap<String, Value> hashMap = new HashMap<>();

	private SymbolTable() {

	}

	public static SymbolTable newInstance(ArrayList<ClassVariableDeceleration> variableDecelerations) {

		SymbolTable newVal = new SymbolTable();

		int objectArgs = 0;
		int staticArgs = 0;
		for (ClassVariableDeceleration variableDeceleration : variableDecelerations) {
			final String type = variableDeceleration.getType();
			final Segment segment = variableDeceleration.getSegment();

			for (String name : variableDeceleration.getNames()) {

				int index = 0;
				switch (segment) {
					case STATIC:
						index = staticArgs++;
						break;
					case THIS:
						index = objectArgs++;
						break;
				}
				
				final Value value = new Value(segment, type, index);
				newVal.hashMap.put(name, value);
			}
		}

		return newVal;

	}

	public static SymbolTable newInstance(ArrayList<SubroutineVariableDeceleration> variableDecelerations, int
			argumentCountStart, SubroutineParameterList parameterList) {

		SymbolTable newVal = new SymbolTable();


		int localArgs = 0;
		for (SubroutineVariableDeceleration variableDeceleration : variableDecelerations) {

			final String type = variableDeceleration.getType();
			final Segment segment = Segment.LOCAL;

			for (String name : variableDeceleration.getNames()) {

				final Value value = new Value(segment, type, localArgs++);
				newVal.hashMap.put(name, value);
			}

		}


		final int[] i = {argumentCountStart};
		parameterList.forEach(tuple -> newVal.hashMap.put(tuple.y.getValue(), new Value(Segment.ARGUMENT, tuple.x
				.getValue(), i[0]++)));

		return newVal;


	}

	public int size() {

		return hashMap.size();
	}

	public Value get(JackToken name) {

		return hashMap.get(name.getValue());
	}

	public Value get(String value) {

		return hashMap.get(value);

	}


	public static class Value {

		public Segment segment;

		public String type;

		public int index;

		public Value(Segment segment, String type, int index) {

			this.segment = segment;
			this.type = type;
			this.index = index;
		}

		@Override
		public String toString() {

			return "Value{" +
					"segment=" + segment +
					", type='" + type + '\'' +
					", index=" + index +
					'}';
		}
	}
}
