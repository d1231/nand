package jack.compiler.vm;

/**
 * Created by danny on 23/04/15.
 */
public enum Segment {
	ARGUMENT,
	LOCAL,
	STATIC,
	CONSTANT,
	THIS,
	THAT,
	POINTER,
	TEMP;

	public String getVM() {

		return this.toString().toLowerCase();

	}
}
