package jack.compiler.vm;

/**
 * Created by danny on 23/04/15.
 */
public enum ArithmeticVMCommand {
	ADD, SUB, NEG, EQ, GT, LT, AND, NOT, OR;

	public String getVM() {

		return toString().toLowerCase();
	}
}
