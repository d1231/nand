package jack.compiler.vm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by danny on 13/04/15.
 */
public class JackVMWriter implements AutoCloseable {

	private static final String PUSH = "push %s %d";

	private static final String POP = "pop %s %d";

	private static final String LABEL = "label %s";

	private static final String RETURN = "return";

	private static final String CALL = "call %s %d";

	private static final String FUNCTION_DECLRATION = "function %s %d";

	private static final String GOTO = "goto %s";

	private static final String IF_GOTO = "if-goto %s";


	private final BufferedWriter bufferedWriter;


	public JackVMWriter(File file) throws IOException {

		this.bufferedWriter = new BufferedWriter(new FileWriter(file));
	}

	@Override
	public void close() throws Exception {

		bufferedWriter.close();
	}


	public void writePush(Segment segment, int index) throws IOException {

		writeLine(String.format(PUSH, segment.getVM(), index));

	}

	private void writeLine(String str) throws IOException {

		bufferedWriter.write(str);
		bufferedWriter.newLine();

	}

	public void writePop(Segment segment, int index) throws IOException {

		writeLine(String.format(POP, segment.getVM(), index));

	}

	public void writeLabel(String label) throws IOException {

		writeLine(String.format(LABEL, label));
	}

	public void writeReturn() throws IOException {

		writeLine(RETURN);
	}

	public void writeCall(String funcName, int argument) throws IOException {

		writeLine(String.format(CALL, funcName, argument));
	}

	public void writeFunction(String funcName, int argument) throws IOException {

		writeLine(String.format(FUNCTION_DECLRATION, funcName, argument));
	}

	public void writeGoto(String label) throws IOException {

		writeLine(String.format(GOTO, label));
	}

	public void writeIfGoto(String label) throws IOException {

		writeLine(String.format(IF_GOTO, label));
	}

	public void writeArithmetic(ArithmeticVMCommand command) throws IOException {

		writeLine(command.getVM());
	}

	public void nextLine() throws IOException {

		writeLine("");

	}
}
