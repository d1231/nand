package jack.compiler;

import jack.compiler.jack_class.SymbolTable;

/**
 * Created by danny on 23/04/15.
 */
public class JackClassContext {

	private final SymbolTable symbolTable;

	private final String fileName;

	private final int size;

	public JackClassContext(SymbolTable symbolTable, String fileName, int size) {

		this.symbolTable = symbolTable;
		this.fileName = fileName;
		this.size = size;
	}

	public SymbolTable getSymbolTable() {

		return symbolTable;
	}

	public String getFileName() {

		return fileName;
	}

	public int getSize() {

		return size;
	}
}
