package jack.compiler;

import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackToken;

import java.io.IOException;

/**
 * Created by danny on 17/04/15.
 */
public interface JackTokenFeeder {

	JackToken peekNextNext() throws IOException, JackTokenizerException;

	/**
	 *
	 * @return next toekn
	 */
	JackToken consumeNext() throws IOException, JackTokenizerException;

	JackToken peekNext() throws IOException, JackTokenizerException;

}
