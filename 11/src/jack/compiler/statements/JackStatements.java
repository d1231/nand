package jack.compiler.statements;

import jack.compiler.JackElement;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by danny on 19/04/15.
 */
public class JackStatements implements JackElement, Iterable<JackStatement> {

	private final ArrayList<JackStatement> statements;

	public JackStatements(ArrayList<JackStatement> statements) {

		this.statements = statements;

	}

	@Override
	public Iterator<JackStatement> iterator() {

		return statements.iterator();
	}
}
