package jack.compiler.statements.statement;

import jack.compiler.JackSubroutineContext;
import jack.compiler.expression.JackExpression;
import jack.compiler.jack_class.SymbolTable;
import jack.compiler.statements.JackStatement;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.vm.ArithmeticVMCommand;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class LetStatement implements JackStatement {

	private final JackToken name;

	private final JackExpression arrayExpression;

	private final JackExpression expression;

	public LetStatement(JackToken name, JackExpression arrayExpression, JackExpression expression) {

		this.name = name;
		this.arrayExpression = arrayExpression;
		this.expression = expression;

	}

	@Override
	public String toString() {

		return "LetStatement{" +
				"name=" + name +
				", arrayExpression=" + arrayExpression +
				", expression=" + expression +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		SymbolTable.Value val = subroutineContext.get(name.getValue());
		expression.writeVM(writer, subroutineContext);

		if (arrayExpression != null) {

			writer.writePush(val.segment, val.index);
			arrayExpression.writeVM(writer, subroutineContext);
			writer.writeArithmetic(ArithmeticVMCommand.ADD);
			writer.writePop(Segment.POINTER, 1);
			writer.writePop(Segment.THAT, 0);

		} else {

			writer.writePop(val.segment, val.index);
		}

	}
}
