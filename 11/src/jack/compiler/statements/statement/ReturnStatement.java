package jack.compiler.statements.statement;

import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.expression.JackExpression;
import jack.compiler.statements.JackStatement;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class ReturnStatement implements JackStatement {

	private final JackExpression expression;

	public ReturnStatement(JackExpression expression) {

		this.expression = expression;

	}

	@Override
	public String toString() {

		return "ReturnStatement{" +
				"expression=" + expression +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {


		if (expression != null) {
			expression.writeVM(writer, subroutineContext);
		} else {
			writer.writePush(Segment.CONSTANT, 0);
		}

		writer.writeReturn();

	}
}
