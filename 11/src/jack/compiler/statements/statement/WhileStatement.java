package jack.compiler.statements.statement;

import jack.compiler.vm.ArithmeticVMCommand;
import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.expression.JackExpression;
import jack.compiler.statements.JackStatement;
import jack.compiler.statements.JackStatements;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class WhileStatement implements JackStatement {

	private final JackExpression condition;

	private final JackStatements statements;

	public WhileStatement(JackExpression condition, JackStatements statements) {

		this.condition = condition;

		this.statements = statements;

	}

	@Override
	public String toString() {

		return "WhileStatement{" +
				"condition=" + condition +
				", statements=" + statements +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		String startLabel = subroutineContext.getFreeLabel();
		String endLabel = subroutineContext.getFreeLabel();

		writer.writeLabel(startLabel);

		condition.writeVM(writer, subroutineContext);

		writer.writeArithmetic(ArithmeticVMCommand.NOT);

		writer.writeIfGoto(endLabel);

		for (JackStatement statement : statements) {
			statement.writeVM(writer, subroutineContext);
		}

		writer.writeGoto(startLabel);

		writer.writeLabel(endLabel);
	}
}
