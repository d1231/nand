package jack.compiler.statements.statement;

import jack.compiler.vm.ArithmeticVMCommand;
import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.expression.JackExpression;
import jack.compiler.statements.JackStatement;
import jack.compiler.statements.JackStatements;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class IfStatement implements JackStatement {

	private final JackExpression condition;

	private final JackStatements statements;

	private final JackStatements elseStatements;

	public IfStatement(JackExpression condition, JackStatements statements, JackStatements elseStatements) {

		this.condition = condition;

		this.statements = statements;

		this.elseStatements = elseStatements;

	}

	@Override
	public String toString() {

		return "IfStatement{" +
				"condition=" + condition +
				", statements=" + statements +
				", elseStatements=" + elseStatements +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		String elseLabel = subroutineContext.getFreeLabel();
		String endLabel = subroutineContext.getFreeLabel();

		condition.writeVM(writer, subroutineContext);

		writer.writeArithmetic(ArithmeticVMCommand.NOT);

		writer.writeIfGoto(elseLabel);

		for (JackStatement statement : statements) {
			statement.writeVM(writer, subroutineContext);
		}

		writer.writeGoto(endLabel);

		writer.writeLabel(elseLabel);

		if (elseStatements != null) {

			for (JackStatement elseStatement : elseStatements) {
				elseStatement.writeVM(writer, subroutineContext);
			}
		}

		writer.writeLabel(endLabel);
	}
}
