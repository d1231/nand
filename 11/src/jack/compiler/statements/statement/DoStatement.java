package jack.compiler.statements.statement;

import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.statements.JackStatement;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 19/04/15.
 */
public class DoStatement implements JackStatement {

	private final JackTerm subroutineCall;

	public DoStatement(JackTerm subroutineCall) {

		this.subroutineCall = subroutineCall;

	}

	@Override
	public String toString() {

		return "DoStatement{" +
				"subroutineCall=" + subroutineCall +
				'}';
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		subroutineCall.writeVM(writer, subroutineContext);
		writer.writePop(Segment.TEMP, 0);
	}
}
