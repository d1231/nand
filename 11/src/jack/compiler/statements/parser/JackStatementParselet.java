package jack.compiler.statements.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.statements.JackStatement;
import jack.compiler.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public interface JackStatementParselet {

	JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException;


}
