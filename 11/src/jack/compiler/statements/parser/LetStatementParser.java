package jack.compiler.statements.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.JackExpression;
import jack.compiler.expression.JackExpressionParser;
import jack.compiler.statements.JackStatement;
import jack.compiler.statements.JackStatementParserException;
import jack.compiler.statements.statement.LetStatement;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackKeyword;
import jack.compiler.tokenizer.token.JackToken;
import jack.compiler.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class LetStatementParser implements JackStatementParselet {

	private static final JackToken letToken = JackKeyword.factory("let");

	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		LetStatementBuilder builder = new LetStatementBuilder();

		JackToken token = jackTokenFeeder.consumeNext();

		if (!token.equals(letToken)) {
			throw new JackStatementParserException();
		}

		builder.setName(jackTokenFeeder.consumeNext());

		// parse [ expression ] if there is
		if (JackUtils.isOpeningSquareBracket(jackTokenFeeder.peekNext())) {

			jackTokenFeeder.consumeNext();

			builder.setArrayExpression(JackExpressionParser.parse(jackTokenFeeder));

			if (!JackUtils.isClosingSquareBracket(jackTokenFeeder.consumeNext())) {
				throw new JackStatementParserException();
			}

		}

		if (!JackUtils.isEqualSign(jackTokenFeeder.consumeNext())) {

			throw new JackStatementParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);
		builder.setExpression(expression);

		if (!JackUtils.isEndToken(jackTokenFeeder.consumeNext())) {

			throw new JackStatementParserException();
		}

		return builder.build();
	}

	private static class LetStatementBuilder {

		private JackToken name;

		private JackExpression arrayExpression = null;

		private JackExpression expression;

		public void setName(JackToken name) {

			this.name = name;
		}

		public void setArrayExpression(JackExpression arrayExpression) {

			this.arrayExpression = arrayExpression;
		}

		public void setExpression(JackExpression expression) {

			this.expression = expression;
		}

		public JackStatement build() {

			return new LetStatement(name, arrayExpression, expression);
		}
	}
}
