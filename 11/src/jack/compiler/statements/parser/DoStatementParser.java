package jack.compiler.statements.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.term.JackTerm;
import jack.compiler.expression.term.parser.JackTermParselet;
import jack.compiler.statements.JackStatement;
import jack.compiler.statements.JackStatementParserException;
import jack.compiler.statements.statement.DoStatement;
import jack.compiler.subroutine.call.SubRoutineCallParselet;
import jack.compiler.subroutine.call.SubroutineInvokeParselet;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackKeyword;
import jack.compiler.utils.JackUtils;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 18/04/15.
 */
public class DoStatementParser implements JackStatementParselet {

	private static final JackKeyword DO = JackKeyword.factory("do");

	private final static HashMap<String, JackTermParselet> parselets = new HashMap<String, JackTermParselet>() {{

		put(".", new SubroutineInvokeParselet(false));
		put("(", new SubRoutineCallParselet(false));

	}};


	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException,
			JackParserException {

		if (!DO.equals(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}


		JackTermParselet parselet = parselets.get(jackTokenFeeder.peekNextNext().getValue());

		if (parselet == null) {
			throw new JackStatementParserException();
		}

		JackTerm subroutineCall = parselet.parse(jackTokenFeeder);

		if (!JackUtils.isEndToken(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		return new DoStatement(subroutineCall);

	}
}
