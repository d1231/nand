package jack.compiler.statements.parser;

import jack.compiler.JackParserException;
import jack.compiler.JackTokenFeeder;
import jack.compiler.expression.JackExpression;
import jack.compiler.expression.JackExpressionParser;
import jack.compiler.statements.JackStatement;
import jack.compiler.statements.JackStatementParserException;
import jack.compiler.statements.statement.ReturnStatement;
import jack.compiler.tokenizer.JackTokenizerException;
import jack.compiler.tokenizer.token.JackKeyword;
import jack.compiler.utils.JackUtils;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public class ReturnStatementParser implements JackStatementParselet {

	private static final JackKeyword RETURN = JackKeyword.factory("return");

	@Override
	public JackStatement parseStatement(JackTokenFeeder jackTokenFeeder) throws IOException, JackTokenizerException, JackParserException {

		if (!RETURN.equals(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		JackExpression expression = JackExpressionParser.parse(jackTokenFeeder);

		if (!JackUtils.isEndToken(jackTokenFeeder.consumeNext())) {
			throw new JackStatementParserException();
		}

		return new ReturnStatement(expression);

	}
}
