package jack.compiler.statements;

import jack.compiler.JackElement;
import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;

import java.io.IOException;

/**
 * Created by danny on 18/04/15.
 */
public interface JackStatement extends JackElement {

	void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException;
}
