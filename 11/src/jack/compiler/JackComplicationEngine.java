package jack.compiler;

import jack.compiler.jack_class.JackClass;
import jack.compiler.jack_class.JackClassParser;
import jack.compiler.tokenizer.JackTokenizerException;

import java.io.IOException;

/**
 * Created by danny on 13/04/15.
 */
public class JackComplicationEngine {

	private final JackTokenFeeder feeder;

	public JackComplicationEngine(JackTokenFeeder feeder) {

		this.feeder = feeder;

	}

	public JackClass createClass() throws JackParserException, IOException, JackTokenizerException {

		return JackClassParser.parse(feeder);
	}
}
