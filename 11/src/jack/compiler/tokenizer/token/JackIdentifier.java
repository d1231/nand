package jack.compiler.tokenizer.token;


import jack.compiler.JackSubroutineContext;
import jack.compiler.jack_class.SymbolTable;
import jack.compiler.vm.JackVMWriter;

import java.io.IOException;

/**
 * Created by danny on 17/04/15.
 */
public class JackIdentifier extends JackToken {

	public JackIdentifier(String value) {

		super(JackTokenType.IDENTIFIER, value);
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		SymbolTable.Value val = subroutineContext.get(getValue());

		writer.writePush(val.segment, val.index);

	}
}
