package jack.compiler.tokenizer.token;

import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 17/04/15.
 */
public class JackIntegerConstant extends JackToken {

	private final int val;

	public JackIntegerConstant(int value) {

		super(JackTokenType.INTEGER_CONSTANT, String.valueOf(value));

		val = value;
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		writer.writePush(Segment.CONSTANT, val);
	}
}
