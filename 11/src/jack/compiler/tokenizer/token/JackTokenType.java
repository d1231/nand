package jack.compiler.tokenizer.token;

/**
 * Created by danny on 13/04/15.
 */
public enum JackTokenType {

	KEYWORD,
	SYMBOL,
	INTEGER_CONSTANT,
	STRING_CONSTANT,
	IDENTIFIER

}
