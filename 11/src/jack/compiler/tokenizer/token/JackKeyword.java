package jack.compiler.tokenizer.token;

import jack.compiler.vm.ArithmeticVMCommand;
import jack.compiler.JackSubroutineContext;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by danny on 17/04/15.
 */
public class JackKeyword extends JackToken {

	private static final String KEYWORD = "keyword";

	private final static HashMap<String, JackKeyword> keywordMap = new HashMap<String, JackKeyword>() {{

		put("class", new JackKeyword("class"));
		put("constructor", new JackKeyword("constructor"));
		put("function", new JackKeyword("function"));
		put("method", new JackKeyword("method"));
		put("field", new JackKeyword("field"));
		put("static", new JackKeyword("static"));
		put("var", new JackKeyword("var"));
		put("int", new JackKeyword("int"));
		put("char", new JackKeyword("char"));
		put("boolean", new JackKeyword("boolean"));
		put("void", new JackKeyword("void"));
		put("true", new JackKeyword("true"));
		put("false", new JackKeyword("false"));
		put("null", new JackKeyword("null"));
		put("this", new JackKeyword("this"));
		put("let", new JackKeyword("let"));
		put("do", new JackKeyword("do"));
		put("if", new JackKeyword("if"));
		put("else", new JackKeyword("else"));
		put("while", new JackKeyword("while"));
		put("return", new JackKeyword("return"));

	}};

	private JackKeyword(String value) {

		super(JackTokenType.KEYWORD, value);
	}

	public static Iterable<? extends String> getAllSymbols() {

		return keywordMap.keySet();
	}

	public static JackKeyword factory(String value) {

		return keywordMap.get(value);

	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

		switch (getValue()) {
			case "null":case "false":
				writer.writePush(Segment.CONSTANT, 0);
				break;
			case "true":
				writer.writePush(Segment.CONSTANT, 0);
				writer.writeArithmetic(ArithmeticVMCommand.NOT);
				break;
			case "this":
				writer.writePush(Segment.POINTER, 0);
				break;
		}

	}
}
