package jack.compiler.tokenizer.token;


import jack.compiler.JackSubroutineContext;
import jack.compiler.tokenizer.JackTokenizer;
import jack.compiler.vm.JackVMWriter;
import jack.compiler.vm.Segment;

import java.io.IOException;

/**
 * Created by danny on 17/04/15.
 */
public class JackStringConstant extends JackToken {


	private static final String STRING_NEW = "String.new";

	private static final String STRING_APPEND_CHAR = "String.appendChar";

	public JackStringConstant(String value) {

		super(JackTokenType.STRING_CONSTANT, value);
	}

	@Override
	public void writeVM(JackVMWriter writer, JackSubroutineContext subroutineContext) throws IOException {

        String str = cleanConstant(getValue());

        writer.writePush(Segment.CONSTANT, str.length());
        writer.writeCall(STRING_NEW, 1);

        for (int i = 0; i < str.length(); i++) {

            char c = str.charAt(i);
            writer.writePush(Segment.CONSTANT, c);
            writer.writeCall(STRING_APPEND_CHAR, 2);

        }

    }

    private String cleanConstant(String value) {

        value = value.replaceAll("\t", "\\\\t");
        value = value.replace((char) JackTokenizer.TAB_REPLACEMENT, '\t');
        value = value.replaceAll("\r", "\\\\r");
        value = value.replaceAll("\n", "\\\\n");
        return value;
    }
}
