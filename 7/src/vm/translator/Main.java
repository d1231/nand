package vm.translator;


/**
 * Created by danny on 22/03/15.
 */
public class Main {


	public static void main(String[] args) {

		if (args.length == 0) {
			System.err.println("Please provide argument");
			return;
		}

		String arg = args[0];

		VMTranslator VMTranslator = new VMTranslator(arg);

		try {
			String file = VMTranslator.createASMFile();

			System.out.println("OK create asm file: " + file);
		} catch (Exception e) {
			System.err.println("ERROR: " + e.toString());
		}


	}

}
