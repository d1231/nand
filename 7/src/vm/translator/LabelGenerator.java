package vm.translator;

/**
 * Created by danny on 21/03/15.
 */
public class LabelGenerator {

	public static final String LABEL = "LABEL";

	private int count = 1;

	public String getLabel() {

		return LABEL + count++;
	}
}
