package vm.translator.command;

import vm.translator.VMContext;

/**
 * Defines an interface for hack virtual machine command
 * <p>
 * Created by danny on 20/03/15.
 */
public interface VMCommand {

	public String toAssembly(VMContext context);
}
