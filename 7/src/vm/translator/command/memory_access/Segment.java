package vm.translator.command.memory_access;

/**
 * Created by danny on 21/03/15.
 */
public enum Segment {
	ARGUMENT,
	LOCAL,
	STATIC,
	CONSTANT,
	THIS,
	THAT,
	POINTER,
	TEMP;

	private static final String ARG_SEGMENT = "R2";

	private static final String LOCAL_SEGMENT = "R1";

	private static final String THIS_SEGMENT = "R3";

	private static final String THAT_SEGMENT = "R4";

	private static final String POINTER_SEGMENT = "R3";

	private static final String TEMP_SEGMENT = "R5";

	public static Segment getSegment(String seg) {

		return Segment.valueOf(seg.toUpperCase());
	}

	public String getSegment() {

		switch (this) {
			case ARGUMENT:
				return ARG_SEGMENT;
			case LOCAL:
				return LOCAL_SEGMENT;
			case THIS:
				return THIS_SEGMENT;
			case THAT:
				return THAT_SEGMENT;
			case POINTER:
				return POINTER_SEGMENT;
			case TEMP:
				return TEMP_SEGMENT;

		}
		return null;
	}
}
