package vm.translator.command.arithmetic;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 21/03/15.
 */
public class BinaryArithmeticCommand implements VMCommand {

	private static final String COMMAND = "@SP\n" +
			"AM=M-1\n" +
			"D=M\n" +
			"A=A-1\n" +
			"%s";

	private final BinaryOperator operator;

	public BinaryArithmeticCommand(BinaryOperator operator) {

		this.operator = operator;
	}

	@Override
	public String toAssembly(VMContext context) {

		return String.format(COMMAND, operator.toAssembly());
	}

	/**
	 * Created by danny on 21/03/15.
	 */
	public static enum BinaryOperator {
		ADD,
		SUB,
		AND,
		OR;

		public String toAssembly() {

			switch (this) {
				case ADD:
					return "M=D+M";
				case SUB:
					return "M=M-D";
				case AND:
					return "M=D&M";
				case OR:
					return "M=D|M";
			}
			return "";
		}
	}
}
