package vm.translator.command.arithmetic;

import vm.translator.VMContext;
import vm.translator.command.VMCommand;

/**
 * Created by danny on 20/03/15.
 */
public class UnaryVMCommand implements VMCommand {

	private static final String COMMAND = "@SP\n" +
			"A=M-1\n" +
			"%s";

	private final UnaryOperator operator;

	public UnaryVMCommand(UnaryOperator operator) {

		this.operator = operator;
	}

	@Override
	public String toAssembly(VMContext context) {

		return String.format(COMMAND, operator.toAssembly());
	}

	/**
	 * Created by danny on 21/03/15.
	 */
	public static enum UnaryOperator {
		NEG, NOT;

		public String toAssembly() {

			switch (this) {
				case NEG:
					return "M=-M";
				case NOT:
					return "M=!M";
			}

			return "";
		}
	}
}
