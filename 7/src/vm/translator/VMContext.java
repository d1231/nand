package vm.translator;

/**
 * Created by danny on 21/03/15.
 */
public class VMContext {

	private final LabelGenerator labelGenerator;

	private final String name;

	public VMContext(String name, LabelGenerator labelGenerator) {

		this.name = name;
		this.labelGenerator = labelGenerator;
	}

	public String getFileName() {

		return name;
	}

	public String getLabel() {

		return labelGenerator.getLabel();
	}

}
