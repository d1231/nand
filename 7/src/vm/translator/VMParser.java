package vm.translator;

import vm.translator.command.VMCommand;
import vm.translator.command.arithmetic.BinaryArithmeticCommand;
import vm.translator.command.arithmetic.JumpArithmeticCommand;
import vm.translator.command.arithmetic.UnaryVMCommand;
import vm.translator.command.memory_access.PopVMCommand;
import vm.translator.command.memory_access.PushVMCommand;
import vm.translator.command.memory_access.Segment;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by danny on 20/03/15.
 */
public class VMParser implements AutoCloseable {

	private static final String POP = "pop";

	private static final String PUSH = "push";

	private static final String COMMENT_REGEX = "//.*";

	private static final Pattern commentPattern = Pattern.compile(COMMENT_REGEX);

	private final static Map<String, VMCommand> arithmeticTable = new HashMap<String, VMCommand>() {{

		put("add", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.ADD));
		put("sub", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.SUB));
		put("neg", new UnaryVMCommand(UnaryVMCommand.UnaryOperator.NEG));
		put("eq", new JumpArithmeticCommand(JumpArithmeticCommand.JumpOperator.EQUAL));
		put("gt", new JumpArithmeticCommand(JumpArithmeticCommand.JumpOperator.GREATER_THAN));
		put("lt", new JumpArithmeticCommand(JumpArithmeticCommand.JumpOperator.SMALLER_THAN));
		put("and", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.AND));
		put("or", new BinaryArithmeticCommand(BinaryArithmeticCommand.BinaryOperator.OR));
		put("not", new UnaryVMCommand(UnaryVMCommand.UnaryOperator.NOT));

	}};

	private final BufferedReader reader;

	private String line;

	/**
	 * Create VM file Parser from file
	 *
	 * @param file
	 * @throws FileNotFoundException
	 */
	public VMParser(File file) throws FileNotFoundException {

		reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
	}

	public boolean moveToNext() throws IOException {

		do {

			line = reader.readLine();

			if (line == null) {
				return false;
			}

			line = line.trim();

			// skip if empty
			if (line.isEmpty()) {
				continue;
			}

			// skip if all line is commented
			Matcher m = commentPattern.matcher(line);
			if (m.matches()) {
				continue;

			}

			// remove inline comment
			if (m.find()) {
				line = line.substring(0, m.start());
			}

			return true;

		} while (true);

	}

	public VMCommand get() throws VMFileException {

		VMCommand command = VMParser.parseArithmeticCommand(line);
		if (command != null) {
			return command;
		}

		command = VMParser.parseMemoryAccessCommand(line);
		if (command != null) {
			return command;
		}

		throw new VMFileException("Couldn't parse: " + line);

	}

	private static VMCommand parseMemoryAccessCommand(String line) {

		String[] arr = line.split(" ");

		if (arr.length != 3) {
			return null;
		}

		if (arr[0].equals(POP)) {
			return new PopVMCommand(Segment.getSegment(arr[1]), arr[2]);
		}

		if (arr[0].equals(PUSH)) {
			return new PushVMCommand(Segment.getSegment(arr[1]), arr[2]);
		}

		return null;
	}

	private static VMCommand parseArithmeticCommand(String line) {

		return arithmeticTable.getOrDefault(line, null);

	}

	@Override
	public void close() throws Exception {

		reader.close();

	}
}
