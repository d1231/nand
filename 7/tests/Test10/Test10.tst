// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/07/MemoryAccess/BasicTest/BasicTest.tst

load Test10.asm,
output-file Test10.out,
compare-to Test10.cmp,
output-list RAM[0]%D1.6.1 RAM[256]%D1.6.1 RAM[257]%D1.6.1  
            RAM[258]%D1.6.1 RAM[259]%D1.6.1 RAM[260]%D1.6.1 
            RAM[261]%D1.6.1 RAM[262]%D1.6.1 RAM[263]%D1.6.1
            RAM[264]%D1.6.1;

set RAM[0] 256,


repeat 600 {
  ticktock;
}

output;
